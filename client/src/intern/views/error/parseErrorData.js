export default ( status, data, msg ) => {

	let header,
		message;

	switch (status) {
		default:
		header = 'Foutmelding';
		message = 'Er ging iets fout met het laden van deze pagina. Neem contact op met uw beheerder voor meer informatie';
		break;

		case 404:
		header = 'Niet gevonden';
		message = 'Dit object kon niet worden gevonden. Mogelijk is het verwijderd.';
		break;
	}

	return {
		header,
		message: msg || message
	};

};
