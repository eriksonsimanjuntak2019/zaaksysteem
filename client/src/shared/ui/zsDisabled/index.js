import angular from 'angular';

export default
  angular.module('zsDisabled', [
  ])
  .directive('zsDisabled', () => {
    return {
      restrict: 'A',
      link ( scope, element, attr ) {
        let value = false;
        const preventDefaultHandler = (event) => value && event.preventDefault();
        
        element.on('click', preventDefaultHandler);

        scope.$watch(attr.zsDisabled, (updatedValue) => {
          element.attr('aria-disabled', updatedValue);
          element.toggleClass('disabled', updatedValue);
          value = updatedValue;
        });

        scope.$on('$destroy', () => {
          element.off('click', preventDefaultHandler);
        });
      },
    };

  })
    .name;
