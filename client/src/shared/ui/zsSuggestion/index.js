import angular from 'angular';
import template from './template.html';

export default
	angular.module('shared.ui.zsSuggestion', [])
		.directive('zsAutoComplete', [ ( ) => {

			return {
				restrict: 'E',
				template,
				scope: {

				},
				bindToController: true,
				controller: [ function ( ) {

				}],
				controllerAs: 'zsSuggestion'
			};

		}])
		.name;
