const { readFileSync } = require('fs');
const cherryPack = require('../../library/cherryPack');
const { DEV_SERVER_HOSTNAME, DEV_SERVER_PORT } = require('../../library/constants');
const stats = require('./stats');

const read = baseName => readFileSync(`/etc/nginx/ssl/${baseName}`, 'utf8');

module.exports = cherryPack({
  development() {
    const key = read('server.key');
    const cert = read('server.crt');
    const ca = read('ca.key');

    return {
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      host: DEV_SERVER_HOSTNAME,
      hot: true,
      https: {
        key,
        cert,
        ca,
      },
      inline: true,
      port: DEV_SERVER_PORT,
      stats,
    };
  },
});
