#!/usr/bin/perl -w

use Moose;
use Data::Dumper;

use Cwd 'realpath';

use FindBin;
use lib "$FindBin::Bin/../../lib";

use Catalyst qw[ConfigLoader];

use Catalyst::Log;
use Catalyst::Model::DBIC::Schema;

my $log = Catalyst::Log->new;

error("USAGE: $0 [dsn] [user] [password] [commit]") unless @ARGV && scalar(@ARGV) >= 3;

my ($dsn, $user, $password, $commit) = @ARGV;
my $dbic = database($dsn, $user, $password);

$dbic->txn_do(sub {
    fill_case_relations($dbic);
});

$log->info('All done.');
$log->_flush;

sub database {
    my ($dsn, $user, $password) = @_;

    Catalyst::Model::DBIC::Schema->config(
        schema_class => 'Zaaksysteem::Schema',
        connect_info => {
            dsn => $dsn,
            pg_enable_utf8 => 1,
            user => $user,
            password => $password
        }
    );

    Catalyst::Model::DBIC::Schema->new->schema;
}

sub error {
    $log->error(shift);
    $log->_flush;

    exit;
}

sub info {
    $log->info(sprintf shift, @_);
    $log->_flush;
}

sub fill_case_relations {
    my $dbic = shift;

    my $cases = $dbic->resultset('Zaak');

    while(my $zaak = $cases->next) {
        info('Processing case %d', $zaak->id);

        my @relations;

        push(@relations, try_relations($dbic, $zaak));
        push(@relations, try_continuation($dbic, $zaak));

        my $count = 1;
        for my $relation (@relations) {
            $relation->order_seq_a($count);
            $relation->order_seq_b($count);

            $count++;
        }

        map { $_->insert; } @relations;
    }
}

sub try_relations {
    my $dbic = shift;
    my $case = shift;

    my $relations = $dbic->resultset('CaseRelation');

    return unless($case->get_column('relates_to'));

    $relations->new_result({
        case_id_a => $case->id,
        case_id_b => $case->get_column('relates_to'),
        type_a => 'plain',
        type_b => 'plain'
    });
}

sub try_continuation {
    my $dbic = shift;
    my $case = shift;

    my $relations = $dbic->resultset('CaseRelation');

    return unless($case->get_column('vervolg_van'));

    $relations->new_result({
        case_id_a => $case->id,
        case_id_b => $case->get_column('vervolg_van'),
        type_a => 'continuation',
        type_b => 'initiator'
    });
}
