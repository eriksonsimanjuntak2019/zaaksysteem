BEGIN;

ALTER TABLE bedrijf ADD COLUMN vestiging_huisletter TEXT;
ALTER TABLE bedrijf ADD COLUMN correspondentie_huisletter TEXT;

ALTER TABLE gm_bedrijf ADD COLUMN vestiging_huisletter TEXT;
ALTER TABLE gm_bedrijf ADD COLUMN correspondentie_huisletter TEXT;

COMMIT;