BEGIN;

ALTER TABLE config ALTER COLUMN value TYPE TEXT;
ALTER TABLE config ALTER COLUMN value TYPE TEXT;

INSERT INTO config (parameter, advanced)
    SELECT
        'new_user_template', TRUE 
    WHERE NOT EXISTS (
        SELECT * FROM CONFIG WHERE parameter = 'new_user_template'
    );

INSERT INTO config (parameter, value, advanced)
    SELECT
        'first_login_intro', 'Welkom!

Voordat je gebruik kan maken van zaaksysteem, zal je afdeling goedgekeurd moeten worden door de gebruikersbeheerder van je gemeente. Dit kan je handig doen door hieronder de gewenste afdeling te selecteren alvorens op volgende te klikken.

Zodra je afdeling is goedgekeurd door de gebruikersbeheerder, ontvang je een e-mail ter bevestiging en kan je gebruikmaken van zaaksysteem.
        ', TRUE 
    WHERE NOT EXISTS (
        SELECT * FROM CONFIG WHERE parameter = 'first_login_intro'
    );

INSERT INTO config (parameter, value, advanced)
    SELECT
        'first_login_confirmation', 'Bedankt!

Zojuist hebben wij je aangemeld bij de gebruikersbeheerder, welke jouw gegevens en je afdeling zal controleren. Dit kan enige tijd in beslag nemen, dus wij zullen je een e-mailtje versturen zodra de gebruikersbeheerder je aanmelding heeft geaccepteerd.
', TRUE 
    WHERE NOT EXISTS (
        SELECT * FROM CONFIG WHERE parameter = 'first_login_confirmation'
    );

COMMIT;
