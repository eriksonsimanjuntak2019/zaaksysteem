BEGIN;

    ALTER TABLE config DROP COLUMN definition_id;
    ALTER TABLE config DROP COLUMN uuid;

COMMIT;
