BEGIN;

    CREATE INDEX ON zaaktype(bibliotheek_categorie_id);
    CREATE INDEX ON bibliotheek_kenmerken(bibliotheek_categorie_id);
    CREATE INDEX ON bibliotheek_notificaties(bibliotheek_categorie_id);
    CREATE INDEX ON bibliotheek_sjablonen(bibliotheek_categorie_id);
    CREATE INDEX ON object_bibliotheek_entry(bibliotheek_categorie_id);

    CREATE INDEX ON zaaktype(deleted);
    CREATE INDEX ON bibliotheek_kenmerken(deleted);
    CREATE INDEX ON bibliotheek_notificaties(deleted);
    CREATE INDEX ON bibliotheek_sjablonen(deleted);

    CREATE INDEX ON bibliotheek_categorie(pid);
    CREATE INDEX ON bibliotheek_kenmerken(magic_string);
    CREATE INDEX ON zaaktype(zaaktype_node_id);
    CREATE INDEX ON zaaktype_node(zaaktype_id);

COMMIT;
