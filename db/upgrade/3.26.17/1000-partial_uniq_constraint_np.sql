BEGIN;
    -- Be able to rerun this sql
    DROP INDEX IF EXISTS burgerservicenummer;

    ALTER TABLE natuurlijk_persoon ALTER COLUMN authenticated SET DEFAULT FALSE;
    UPDATE natuurlijk_persoon set authenticated = false where authenticated is null;
    ALTER TABLE natuurlijk_persoon ALTER COLUMN authenticated SET NOT NULL;

    -- Wipe out weirdish BSN's
    UPDATE natuurlijk_persoon set burgerservicenummer = null where burgerservicenummer = '';
    UPDATE natuurlijk_persoon set burgerservicenummer = null where burgerservicenummer::int = 0;
    UPDATE natuurlijk_persoon set burgerservicenummer = null where burgerservicenummer ~ '[^0-9]';

    -- Create a partial unique index:
    -- You can create an "authentic" person and a non-authentic person,
    -- but you can't create them twice.
    CREATE UNIQUE INDEX burgerservicenummer ON natuurlijk_persoon (burgerservicenummer, authenticated) WHERE deleted_on IS NULL;

COMMIT;

