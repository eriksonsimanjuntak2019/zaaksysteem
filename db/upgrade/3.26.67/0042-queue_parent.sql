BEGIN;

    ALTER TABLE queue
        DROP CONSTRAINT IF EXISTS queue_parent_id_fkey,
        ADD CONSTRAINT queue_parent_id_fkey FOREIGN KEY(parent_id) REFERENCES queue(id) ON DELETE SET NULL;

    CREATE INDEX IF NOT EXISTS queue_status_idx ON queue(status);
    CREATE INDEX IF NOT EXISTS queue_date_created_idx ON queue(date_created);
    CREATE INDEX IF NOT EXISTS queue_date_finished_idx ON queue(date_finished);

COMMIT;
