BEGIN;
    /* ZS-16705_add_pid_column_to_zaaktype_node.sql */

    ALTER TABLE zaaktype_node
        ADD COLUMN moeder_zaaktype_id
            INTEGER
            REFERENCES zaaktype (id)
            ON DELETE SET NULL;

COMMIT;
