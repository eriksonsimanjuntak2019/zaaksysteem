#! /bin/bash

# Install extra dependencies only required during testing
cpanm --with-develop --installdeps .

# The actual test run:
prove -l --formatter TAP::Formatter::Console $@
