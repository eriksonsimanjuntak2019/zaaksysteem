Copy/paste dit naar de "Custom claim rules":

* AD FS Management
* Edit Claim Rules (rechtsklik of rechterbalk)
* Add custom rule
* Paste alles hieronder:

c:[Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsaccountname", Issuer == "AD AUTHORITY"]
 => issue(store = "Active Directory", types = ("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/initials", "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname", "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress", "http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsaccountname", "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/otherphone", "http://schemas.xmlsoap.org/claims/Group", "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/upn", "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname"), query = ";initials,displayName,givenName,mail,sAMAccountName,telephoneNumber,tokenGroups,userPrincipalName,sn;{0}", param = c.Value);
