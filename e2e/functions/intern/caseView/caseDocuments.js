import waitForElement from './../../../functions/common/waitForElement';

export const uploadDocument = documentName => {
    const path = require('path');
    const inputFile = './../../../utilities/documents/text.txt';
    const absolutePath = path.resolve(__dirname, inputFile);

    $('.document-actions input[name="file"]').sendKeys(absolutePath);

    browser.sleep(5000);
};

export const checkDocument = documentName =>
    $$('.object-type-file')
        .map(aDocument =>
            aDocument
                .$('.list-view-entity-title-text .doc-dropdown-menu-item:nth-child(1)')
                .getText()
                .then(name =>
                    name === documentName
                )
        )
        .then(match =>
            match.includes(true)
        );

export const getDocumentSetting = (documentName, setting) =>
    $$('.object-type-file')
        .filter(aDocument =>
            aDocument
                .$('.list-view-entity-title-text .doc-dropdown-menu-item:nth-child(1)')
                .getText()               
                    .then(name =>
                        name === documentName
                    )
        )
        .first()
        .$(setting)
        .getText();

export const getDocumentDescription = documentName =>
    getDocumentSetting(documentName, '.list-view-entity-desc');

export const getDocumentOrigin = documentName =>
    getDocumentSetting(documentName, '.list-view-entity-origin');

export const getDocumentOriginDate = documentName =>
    getDocumentSetting(documentName, '.list-view-entity-origin-date');

export const getDocumentLabels = documentName =>
    $$('.object-type-file')
        .filter(aDocument =>
            aDocument
                .$('.list-view-entity-title-text .doc-dropdown-menu-item:nth-child(1)')
                .getText()
                .then(name =>
                    name === documentName
                )
        )
        .first()
        .$$('.list-view-entity-label')
        .get(0)
        .getText();

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
