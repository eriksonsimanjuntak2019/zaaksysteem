export const getTitle = () =>
    $('.object-view-header-label').getText();

export const getValue = attributeName =>
    $$('.object-field-list .object-field')
        .filter(attribute =>
            attribute
                .$('.object-field-label')
                .getText()
                .then(label =>
                    label === attributeName
                )
        )
        .first()
        .$('.object-field-value')
        .getText();

export const getValues = data =>
    data
        .map(label =>
            getValue(label)
        );

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
