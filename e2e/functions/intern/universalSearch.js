export const universalSearch = $('zs-universal-search');

export const activate = () => {
    universalSearch
        .$('input')
        .click();
};

export const activateByKey = type => {
    if ( type === 'basic' ) {
        browser
            .actions()
            .keyDown(protractor.Key.CONTROL)
            .sendKeys('/')
            .keyDown(protractor.Key.CONTROL)
            .perform();
    } else if ( type === 'actions' ) {
        browser
            .actions()
            .keyDown(protractor.Key.CONTROL)
            .keyDown(protractor.Key.SHIFT)
            .sendKeys('/')
            .keyDown(protractor.Key.CONTROL)
            .keyDown(protractor.Key.SHIFT)
            .perform();
    }
};

export const search = input => {
    activate();
    universalSearch
        .$('input')
        .clear()
        .sendKeys(input);
    browser.sleep(1000);
};

export const exitUniversalSearch = () => {
    universalSearch
        .$('.spot-enlighter-back-button')
        .click();
};

export const openResult = title => {
    const results = universalSearch.$$('.suggestion-list-item');

    results
        .filter(elem =>
            elem.$('.suggestion-text-title').getText().then(text =>
                text === title
            )
        )
        .click();
};

export const getMoreResults = () => {
    universalSearch
        .$('.suggestion-list-group-more-button')
        .click();
};

export const activateContact = title => {
    const results = universalSearch.$$('.suggestion-list-item');

    results
        .each(result =>
            result
                .$('.suggestion-text-title')
                .getText()
                .then(text => {
                    if (text === title) {
                        result
                            .$('.suggestion-actions button')
                            .click();
                    }
                })
        );
};

export const countResults = () =>
    universalSearch
        .$$('.suggestion-list-item')
        .count();

const getTitle = result =>
    result
        .$('.suggestion-text-title')
        .getText();

const getIcon = result =>
    result
        .$('.suggestion > zs-icon')
        .getAttribute('icon-type');

const getLink = result =>
    result
        .$('a')
        .getAttribute('href')
            .then(link =>
                link ? link.replace(browser.baseUrl, '') : undefined
            )

export const getResults = () =>
    universalSearch
        .$$('.suggestion-list-item')
        .map(result =>
            ({
                name: getTitle(result),
                icon: getIcon(result),
                link: getLink(result)
            })
        );

export const openFilterMenu = () => {
    $('zs-dropdown-menu button').click();
};

export const selectFilter = filter => {
    openFilterMenu();
    universalSearch
        .$('zs-dropdown-menu')
        .$$('.popup-menu-list-item')
        .filter(elem =>
            elem
                .getText()
                .then(text =>
                    text === filter
                )
        )
        .click();
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
