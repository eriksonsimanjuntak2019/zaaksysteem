angular.module('Zaaksysteem.admin').directive('zsInstancesDetails', [
	'$q',
	'$interpolate',
	'$parse',
	'zsApi',
	'instancesService',
	'formService',
	'systemMessageService',
	function (
		$q,
		$interpolate,
		$parse,
		api,
		instancesService,
		formService,
		systemMessageService
	) {
		return {
			restrict: 'E',
			scope: {
				instance: '&',
				controlpanelId: '&',
				domain: '@',
				customerConfig: '&',
				onComplete: '&',
				showTemplateOption: '&'
			},
			template:
				'<div' +
					' data-zs-form-template-parser="<[getFormConfig()]>"' +
					' data-zs-form-submit="closePopup()"' +
				'</div>',
			controller: [ '$scope', function ( $scope ) {
				var instance = $scope.instance(),
					formconfig = {
						fields: [
							{
								name: 'id',
								label: 'UUID',
							},
							{
								name: 'fqdn',
								label: 'Webadres',
							},
							{
								name: 'customer_type',
								label: 'Type cloud',
							},
							{
								name: 'software_version',
								label: 'Softwareversie',
							},
							{
								name: 'fallback_url',
								label: 'Maintenance URL',
							},
							{
								name: 'provisioned_on',
								label: 'Datum provisioning',
							},
							{
								name: 'services_domain',
								label: '"Client Side Cert"-domein',
							},
							{
								name: 'api_domain',
								label: 'API domein',
							},
							{
								name: 'database',
								label: 'Database',
							},
							{
								name: 'database_host',
								label: 'Database host',
							},
							{
								name: 'filestore',
								label: 'Storage Bucket',
							},
							{
								name: 'freeform_reference',
								label: 'Vrije referentie',
							},
							{
								name: 'stat_diskspace',
								label: 'Used diskspace (GB)',
							},
						],
					};

				_.each(formconfig.fields, function ( field ) {
					if (instance.instance[field.name] !== undefined) {
						field.value = instance.instance[field.name];
					}

					field.type 		= 'text';
					field.disabled 	= true;
				});

				$scope.getFormConfig = function ( ) {
					return formconfig;
				};
			}]
		};
	}
]);
