/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseAction', [ '$timeout', function ( $timeout ) {
			
			return {			
				controller: [ function ( $scope ) {
					
					var ctrl = this;
					
					ctrl.loading = false;
					
					ctrl.setLoading = function ( loading ) {
						ctrl.loading = loading;
					};
					
					ctrl.isLoading = function ( ) {
						return ctrl.loading;	
					};
					
				}],
				controllerAs: 'caseAction'
			};
			
		}]);
	
})();
