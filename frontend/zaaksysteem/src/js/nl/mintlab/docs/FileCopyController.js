/*

    caseRelatedCases is attached as a dependency to the zsCaseDocumentView module in
    client/src/intern/views/case/zsCaseDocumentView.js

*/

angular.module('Zaaksysteem.docs')
    .controller('nl.mintlab.docs.FileCopyController', [
        '$scope', '$window', 'smartHttp', 'translationService', 'snackbarService', 'caseRelatedCases',
        function ( $scope, $window, smartHttp, translationService, snackbarService, caseRelatedCases) {

        $scope.copyToCase = null;
        $scope.isLoading = false;

        $scope.getType = function() {
            return 'case';
        }

        $scope.onKeyUp = function(query) {
            if (!query || query == '') {
                $scope.copyToCase = null;
            }
        }

        $scope.selectedSuggestion = function(suggestion) {

            if (!suggestion) {
                throw new Error('Received no suggestion-case.');
            }

            $scope.copyToCase = {
                name: suggestion.label,
                number: suggestion.data.id,
                id: suggestion.id
            }
        }

        $scope.startCopy = function() {
           if (!$scope.copyToCase) {
               throw new Error('No case found to copy to.');
           }
           if (!$scope.selectedFiles || $scope.selectedFiles.length == 0) {
               throw new Error('No files found to copy.');
           }

            $scope.isLoading = true;

            function getDutchErrorMessage(type) {
                var errorType = type.split('/').pop();
                var errors = {
                    current_case: 'De geselecteerde zaak is deze zaak.',
                    not_found: 'De geselecteerde zaak kan niet worden gevonden.',
                    closed: 'De geselecteerde zaak is afgehandeld.',
                    unauthorized: 'U heeft niet voldoende rechten om documenten aan deze zaak toe te voegen.',
                    default: 'Onbekend. Neem contact op met de beheerder.'
                }

                return '<br />Foutmelding: ' + (errors[errorType] || errors['default']);
            };

            smartHttp.connect({
                url: '/api/case/' + $scope.caseId + '/file/copy_to_case',
                method: 'POST',
                data: {
                    file_id: $scope.selectedFiles.map(function(file) {
                        return file.id;
                    }),
                    case_id: $scope.copyToCase.number
                },
                blocking: false
            })
            .success(function () {
                snackbarService.info('De document(en) zijn succesvol gekopieerd naar ' + $scope.copyToCase.name + '.');
            })
            .error(function (error) {
                var errorMessage = 'Er is een fout opgetreden bij het kopi&euml;ren.';
                if (error && error.result && error.result.length > 0) {
                    var errorType = error.result[0].type;
                    errorMessage += getDutchErrorMessage(errorType);
                }
                snackbarService.error(errorMessage);
            })
            .finally(function() {
                $scope.copyToCase = null;
                $scope.isLoading = false;
                $scope.closePopup();
            });
        }

        $scope.$watch('copyType', function(){
            if ($scope.copyType !== 'related') return;

            $scope.isLoading = true;

            caseRelatedCases.getRelatedCases($scope.caseId)
                .then(function(relatedCases) {
                    if (relatedCases.length > 0) {
                        $scope.copyToCase = relatedCases[0];
                    }
                    $scope.relatedCases = relatedCases;
                })
                .catch(function(error) {
                    if (error) {
                        snackbarService.error('Er is een fout opgetreden bij het ophalen van de gerelateerde zaken. Foutmelding: ' + error.data.result.instance.message);
                    }
                    $scope.closePopup();
                })
                .finally(function() {
                    $scope.isLoading = false;
                });
        });
    }]);
