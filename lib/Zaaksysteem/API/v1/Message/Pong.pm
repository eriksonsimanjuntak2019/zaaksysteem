package Zaaksysteem::API::v1::Message::Pong;

use Moose;

extends 'Zaaksysteem::API::v1::Message';

=head1 NAME

Zaaksysteem::API::v1::Message::Pong - Model for pong messages for APIv1

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 payload

=cut

has payload => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head1 METHODS

=head2 field_hash

Override for L<Zaaksysteem::API::v1::Message/field_hash> that provides
message type and payload information for the pong message.

    {
        type => 'pong',
        payload => '23FA31B9'
    }

=cut

override field_hash => sub {
    my $self = shift;

    return {
        type => 'pong',
        payload => $self->payload
    }
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
