package Zaaksysteem::API::v1::Status;

use Moose;

=head1 NAME

Zaaksysteem::API::v1::Status - Status message type

=head1 SYNOPSIS

    my $status = Zaaksysteem::API::v1::Status->new(
        version => $my_ver
    );

    $c->stash->{ result } = $status;

=head1 ATTRIBUTES

=head2 version

=cut

has version => (
    is => 'rw',
    isa => 'Str'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
