package Zaaksysteem::CLI::Knife::Casetype;
use Moose::Role;

use BTTW::Tools;
use Data::Dumper;
use JSON;
use Zaaksysteem::CLI::Knife::Action;
use utf8;

my $knife = 'casetype';

=head1 NAME

Zaaksysteem::CLI::Knife::Casetype - Casetype CLI actions

=cut

register_knife $knife => (description => "Casetype related functions");

register_category casetype => (
    knife       => $knife,
    description => "Casetypes",
);

register_action list => (
    knife       => $knife,
    category    => 'casetype',
    description => 'List casetypes',
    run         => sub {
        my $self = shift;
        my (@params) = @_;

        my $p = $self->get_knife_params;

        my %args;
        if (!$p->{deleted}) {
             $args{'me.deleted'} =  undef;
        }

        my $rs = $self->schema->resultset('Zaaktype')->search_rs(
            \%args,
            {
                order_by => { '-desc' => 'me.id' },
                prefetch => 'zaaktype_node_id'
            }
        );

        while (my $k = $rs->next) {
            $self->log->info(
                sprintf("[%d] %s (%s)",
                    $k->id,
                    $k->zaaktype_node_id->titel,
                    # Show either "deleted, active or inactive
                    $k->deleted  ? 'Verwijderd' :
                        $k->active ? 'Actief' : 'Inactief',
                )
            );
        }
    }
);

register_action log => (
    knife       => $knife,
    category    => 'casetype',
    description => 'Get the version history of a casetype',
    run         => sub {
        my $self = shift;
        my (@params) = @_;

        my $p = $self->get_knife_params;
        if (!$p->{id}) {
            throw("zsknife/args", "No ID supplied for your casetype");
        }
        my $casetype = $self->_assert_casetype($p->{id});

        my $logging = $self->_get_logging_by_casetype($casetype);
        my $nodes   = $self->_get_node_by_casetype($casetype);
        while (my $log = $logging->next) {
            my $node = $nodes->next;
            $self->log->info(
                Dumper $self->_fix_log_data_for_node($log, $node));
        }

    }
);

sub _fix_log_by_casetype {
    my ($self, $casetype) = @_;

    my $logging = $self->_get_logging_by_casetype($casetype);
    my $nodes   = $self->_get_node_by_casetype($casetype);

    $self->log->info(
        sprintf(
            "Update logging for casetype %s (%d)",
            $casetype->zaaktype_node_id->titel,
            $casetype->id
        )
    );

    while (my $log = $logging->next) {
        my $node = $nodes->next;
        $self->_fix_log_by_node($log, $node);
    }
}

sub _fix_log_data_for_node {
    my ($self, $log, $node) = @_;

    my $comment = $log->onderwerp // '';

    # Be able to fix up logging even for already touched casetypes
    if ($log->can('data') && exists $log->data->{original_onderwerp}) {
        $comment = $log->data->{original_onderwerp};
    }

    my $user = $log->created_by_name_cache;

    if (!$user) {
        $user = $log->betrokkene_id ? $log->betrokkene_id : $log->created_by;
        if ($user) {
            $user = $self->schema->betrokkene_model->get_by_string($user)
                ->display_name;
        }
        $user //= 'Admin';
    }

    my ($ct_id, $ct_name, $log_line);
    if ($comment =~ /^Zaaktype (\d+) \((.+)\) opgeslagen\.?\s*(.*)/) {
        ($ct_id, $ct_name, $log_line) = ($1, $2, $3);
    }
    elsif ($comment =~ /^Zaaktype (.+) \((\d+)\) is opgeslagen\.?\s*(.*)/) {
        ($ct_id, $ct_name, $log_line) = ($2, $1, $3);
    }

    my $commit_message;
    if ($log_line =~ /Opmerkingen: (.*)/) {
        $commit_message = $1;
        if ($commit_message eq 'Import') {
            $commit_message = "Zaaktype is geïmporteerd";
        }
        $log_line =~ s/Opmerkingen: .*//g;
    }

    my (@components);
    if ($log_line =~ /(?:Componenten gewijzigd|Aangepaste componenten zijn|Aangepast component is): (.+)/
        )
    {
        @components = split(/, /, $1);
        $components[-1] =~ s/\.\s*$//;
        if ($components[-1] =~ /(\w+) en (\w+)/) {
            $components[-1] = $1;
            push(@components, $2);
        }
    }


    return {
        title              => $ct_name,
        case_type          => $ct_id,
        components         => \@components,
        commit_message     => $commit_message,
        original_onderwerp => $comment,
        user               => $user,
    };
}

sub _fix_log_by_node {
    my ($self, $log, $node) = @_;

    my $data = $self->_fix_log_data_for_node($log, $node);
    my $user = delete $data->{user};

    $log->update(
        {
            event_data            => encode_json($data),
            event_type            => 'casetype/mutation',
            created_by_name_cache => $user,
        }
    );

    $node->update({ logging_id => $log->id });
    $self->log->debug(
        sprintf(
            "ZaaktypeNode %d (version %d) logging %d is gewijzigd",
            $node->id, $node->version, $log->id
        )
    );
}

register_action fix_log => (
    knife       => $knife,
    category    => 'casetype',
    description => 'Fix old style logging entries',
    run         => sub {
        my $self = shift;
        my (@params) = @_;

        my $p = $self->get_knife_params;
        if ($p->{id}) {
            my $ct = $self->_assert_casetype($p->{id});
            $self->_fix_log_by_casetype($ct);
        }
        else {
            my $rs = $self->_get_active_casetypes;
            while (my $ct = $rs->next) {
                $self->_fix_log_by_casetype($ct);
            }
        }
    }
);

sub _get_active_casetypes {
    my $self = shift;
    return $self->schema->resultset('Zaaktype')->search_rs(
        { 'me.deleted' => undef },
        {
            order_by => { '-desc' => 'me.id' },
            prefetch => 'zaaktype_node_id'
        }
    );
}

sig _assert_casetype => 'Int';

sub _assert_casetype {
    my ($self, $id) = @_;

    my $ct = $self->schema->resultset('Zaaktype')->search_rs(
        { 'me.id'  => $id, 'me.deleted' => undef },
        { prefetch => 'zaaktype_node_id' }
    )->first;
    return $ct if $ct;

    throw("zsknife/casetype/not_found",
        "No active casetype found with ID $id");
}

sub _get_logging_by_casetype {
    my ($self, $casetype) = @_;

    return $self->schema->resultset('Logging')->search_rs(
        {
            component    => 'zaaktype',
            component_id => $casetype->id,
            event_type   => [undef, 'casetype/mutation'],
        },
        { order_by => { '-desc' => 'id' } }
    );
}

sub _get_node_by_casetype {
    my ($self, $casetype) = @_;
    return $self->schema->resultset('ZaaktypeNode')
        ->search_rs({ zaaktype_id => $casetype->id, },
        { order_by => { '-desc' => 'me.id' }, prefetch => 'zaaktype_id' });
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::CLI::Knife>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
