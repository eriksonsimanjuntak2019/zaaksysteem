package Zaaksysteem::Controller::Beheer::Sysin;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/') : PathPart('beheer/sysin') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');
}

sub overview : Chained('base') : PathPart('overview') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ template } = 'beheer/sysin/overview.tt'
}

sub transactions : Chained('base') : PathPart('transactions') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ template } = 'beheer/sysin/transactions.tt'
}

sub transaction_base : Chained('base') : PathPart('transactions') : CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    $c->stash->{transaction}    = $c->model('DB::Transaction')->find($id);
    $c->stash->{transaction_id} = $id;
}


sub transaction : Chained('transaction_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ template } = 'beheer/sysin/transaction-single.tt'
}

sub record_base : Chained('transaction_base') : PathPart('records') : CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    $c->stash->{record}    = $c->model('DB::TransactionRecord')->find($id);
    $c->stash->{record_id} = $id;
}

sub record_mutation : Chained('record_base') : PathPart('mutation') {
    my ($self, $c, $id) = @_;

    $c->stash->{ mutation_id } = $id;
    $c->stash->{ template } = 'beheer/sysin/mutation-single.tt'
}

sub record : Chained('record_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ template } = 'beheer/sysin/record-single.tt'
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

=head2 overview

TODO: Fix the POD

=cut

=head2 record

TODO: Fix the POD

=cut

=head2 record_base

TODO: Fix the POD

=cut

=head2 record_mutation

TODO: Fix the POD

=cut

=head2 transaction

TODO: Fix the POD

=cut

=head2 transaction_base

TODO: Fix the POD

=cut

=head2 transactions

TODO: Fix the POD

=cut

