package Zaaksysteem::Controller::Object;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/') : PathPart('object'): CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    $c->stash->{ object_id } = $id;
}

sub view : Chained('base'): PathPart(''): Args(0) {

    my ($self, $c ) = @_;

    $c->stash->{ template } = 'object/view.tt'
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

=head2 view

TODO: Fix the POD

=cut

