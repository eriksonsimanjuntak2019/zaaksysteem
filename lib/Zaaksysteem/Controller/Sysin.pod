=head1 NAME

Zaaksysteem::Controller::Sysin - System integration API doc

=head1 SYNOPSIS

 ### Interface settings

 # Creating an interface (POST)
 /sysin/interface/create

 # A list of interfaces (a search) and their metadata (GET)
 /sysin/interface

 # A list of transactions (a search) and their metadata (GET)
 /sysin/transaction

 # View a transaction (GET)
 /sysin/transaction/<ID>

 # Deleting an transaction (POST)
 /sysin/transaction/<ID>/update

=head1 DESCRIPTION

This is the System Integration API documentation. Controller is based on our
ZAPIController.

=head1 SYSIN namespace

Our system integration component of zaaksysteem resides in the sysin namespace
of our module. Every message in and out of our system will run through this
module to keep track of changes when it's send from systems outside
zaaksystee.nl

=head1 SCRUD-H API

For a basic knowledge about our JSON API, please take a look at
L<Zaaksysteem::Manual::API>

=head1 WHERE NEXT?

Now you know the basics about our SYSIN API, please look at the following
sections for detailed usage information.

=head2 INTERFACE

See L<Zaaksysteem::Controller::Sysin::Interface> for an API description
for interface calls

=head2 TRANSACTIONS

See L<Zaaksysteem::Controller::Sysin::Transaction> for an API description
for transaction calls

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
