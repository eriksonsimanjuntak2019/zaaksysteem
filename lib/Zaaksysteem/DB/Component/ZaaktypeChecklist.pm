package Zaaksysteem::DB::Component::ZaaktypeChecklist;

use strict;
use warnings;

use Zaaksysteem::Constants qw/ZAAKSYSTEEM_CONSTANTS/;

#use Moose;

use base qw/DBIx::Class/;

sub added_columns {
    return [qw/
        naam
        type
        options
    /];
}

sub mogelijkheden {
    my ($self) = @_;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->options;
    }
}

sub naam {
    my $self    = shift;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->naam;
    }
}

sub type {
    my $self    = shift;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->value_type;
    }
}

sub label {
    my $self    = shift;

    ### Make sure we write
    return $self->next::method(@_) if @_;

    ### Show label when exists
    return $self->next::method
        if $self->next::method;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->label;
    }
}

sub magic_string {
    my $self    = shift;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->magic_string;
    }
}

sub description {
    my $self    = shift;

    ### Make sure we write
    return $self->next::method(@_) if @_;

    ### Show label when exists
    return $self->next::method
        if $self->next::method;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->description;
    }
}

sub help {
    my $self    = shift;

    ### Make sure we write
    return $self->next::method(@_) if @_;

    ### Show label when exists
    return $self->next::method
        if $self->next::method;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->help;
    }
}

sub document_categorie {
    my $self    = shift;

    ### Make sure we write
    return $self->next::method(@_) if @_;

    ### Show label when exists
    return $self->next::method
        if $self->next::method;

    if ($self->bibliotheek_kenmerken_id) {
        return $self->bibliotheek_kenmerken_id->document_categorie;
    }
}

sub rtkey {
    my $self    = shift;

    my $key = $self->get_column('bibliotheek_kenmerken_id');

    return unless $key;
    return sprintf('kenmerk_id_%s', $key);
}

sub verplicht {
    my $self    = shift;

    return 1 if $self->value_mandatory;

    return;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 added_columns

TODO: Fix the POD

=cut

=head2 description

TODO: Fix the POD

=cut

=head2 document_categorie

TODO: Fix the POD

=cut

=head2 help

TODO: Fix the POD

=cut

=head2 label

TODO: Fix the POD

=cut

=head2 magic_string

TODO: Fix the POD

=cut

=head2 mogelijkheden

TODO: Fix the POD

=cut

=head2 naam

TODO: Fix the POD

=cut

=head2 rtkey

TODO: Fix the POD

=cut

=head2 type

TODO: Fix the POD

=cut

=head2 verplicht

TODO: Fix the POD

=cut

