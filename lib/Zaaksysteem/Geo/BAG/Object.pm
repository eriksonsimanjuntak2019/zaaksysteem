package Zaaksysteem::Geo::BAG::Object;
use Moose;
use namespace::autoclean;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Geo::BAG::Object - BAG Object (as returned by the BAG API)

=head1 SYNOPSIS

    my $bag_object = Zaaksysteem::Geo::BAG::Object->new(bag_object => $object);
    my $woonplaats_naam = $bag_object->woonplaats_as_string;

=head1 DESCRIPTION

The BAG API returns plain JSON objects (= hash refs). This class wraps those
objects to make some convenience methods available for nice displaying/
structuring of the data.

=head1 ATTRIBUTES

=head2 bag_object

The plain JSON BAG object, as returned by the BAG service.

=cut

has bag_object => (
    is => 'ro',
    isa => 'HashRef',
    required => 1,
);

=head1 METHODS

=head2 woonplaats

Returns a hash reference with information about the "Woonplaats" part of the BAG object.

Contains the common keys (see below) and "woonplaats", which is the name of the town.

=cut

sub woonplaats {
    my $self = shift;

    my $woonplaats = $self->bag_object->{woonplaats};

    my %rv = _extract_common_fields($woonplaats);
    $rv{woonplaats} = $woonplaats->{naam};

    return \%rv;
}

=head2 woonplaats_as_string

Returns the "woonplaats" name.

=cut

sub woonplaats_as_string {
    my $self = shift;
    return $self->bag_object->{woonplaats}{naam};
}

=head2 openbareruimte

Returns a hash reference with information about the "Openbare Ruimte" (street)
part of the BAG object.

Contains the common keys (see below), "straat" (the street name) and if
available, "gps_lat_lon", the location of the street (on average).

=cut

sub openbareruimte {
    my $self = shift;
    return unless exists $self->bag_object->{woonplaats}{openbareruimte};

    my $openbareruimte = $self->bag_object->{woonplaats}{openbareruimte};
    my %rv = _extract_common_fields($openbareruimte);

    $rv{straat}      = $openbareruimte->{naam};
    $rv{gps_lat_lon} = $self->bag_object->{geo_lat_lon};

    return \%rv;
}

=head2 openbareruimte_as_string

Returns the street name, prefixed with the town name, in the form
"Town - Street".

=cut

sub openbareruimte_as_string {
    my $self = shift;

    my $openbareruimte = $self->openbareruimte or return;

    return sprintf(
        "%s - %s",
        $self->woonplaats_as_string,
        $openbareruimte->{straat},
    );
}

=head2 nummeraanduiding

Returns a hash reference with information about the "Nummeraanduiding"
(address) part of the BAG object.

Contains the common keys (see below), "postcode", "huisnummer", "huisletter",
"huisnummertoevoeging" and "gps_lat_lon".

=cut

sub nummeraanduiding {
    my $self = shift;
    return unless exists $self->bag_object->{woonplaats}{openbareruimte};
    return unless exists $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding};

    my $nummeraanduiding = $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding};
    my %rv = _extract_common_fields($nummeraanduiding);

    $rv{woonplaats}           = $self->bag_object->{woonplaats}{naam};
    $rv{straat}               = $self->bag_object->{woonplaats}{openbareruimte}{naam};
    $rv{postcode}             = $nummeraanduiding->{postcode};
    $rv{huisnummer}           = $nummeraanduiding->{huisnummer};
    $rv{huisletter}           = $nummeraanduiding->{huisletter};
    $rv{huisnummertoevoeging} = $nummeraanduiding->{huisnummer_toevoeging};
    $rv{gps_lat_lon}          = $self->bag_object->{geo_lat_lon};

    return \%rv;
}

=head2 nummeraanduiding_id

Retrieve the old-style "nummeraanduiding-id" in the form "nummeraanduiding-XXXX", where
XXX is the BAG-Nummeraanduiding identification for this object.

=cut

sub nummeraanduiding_id {
    my $self = shift;
    return unless exists $self->bag_object->{woonplaats}{openbareruimte};
    return unless exists $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding};

    return sprintf(
        'nummeraanduiding-%s',
        $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}{id}
    );
}

=head2 nummeraanduiding_as_string

Returns the address in the form "Town - Street 123a-hs".

=cut

sub nummeraanduiding_as_string {
    my $self = shift;
    
    my $openbareruimte   = $self->openbareruimte or return;
    my $nummeraanduiding = $self->nummeraanduiding or return;

    my $huisnummer = _build_huisnummer($self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding});

    my $rv = sprintf(
        "%s %s, %s%s%s",
        $openbareruimte->{straat},
        $huisnummer,
        ($nummeraanduiding->{postcode} // ''),
        ($nummeraanduiding->{postcode} ? ' ' : ''),
        $nummeraanduiding->{woonplaats},
    );

    return $rv;
}

=head2 verblijfsobject

Returns a hash reference with information about the "Nummeraanduiding"
(address) part of the BAG object.

Contains the common keys (see below), and "surface_area".

=cut

sub verblijfsobject {
    my $self = shift;
    return unless exists $self->bag_object->{woonplaats}{openbareruimte};
    return unless exists $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding};
    return unless exists $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}{verblijfsobject};

    my $vbo = $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}{verblijfsobject};

    my %rv = _extract_common_fields($vbo);
    $rv{surface_area} = $vbo->{oppervlakte};
    $rv{gebruiksdoel} = $vbo->{gebruiksdoel};

    return \%rv;
}

=head2 verblijfsobject

Always returns an empty string ('')

=cut

sub verblijfsobject_as_string {
    return '';
}

=head2 pand

Returns a hash reference with information about the "Nummeraanduiding"
(address) part of the BAG object.

Contains the common keys (see below), and "year_of_construction".

=cut

sub pand {
    my $self = shift;
    return unless exists $self->bag_object->{woonplaats}{openbareruimte};
    return unless exists $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding};
    return unless exists $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}{verblijfsobject};
    return unless exists $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}{verblijfsobject}{pand};

    my $pand = $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}{verblijfsobject}{pand};

    # For now: only extract the first "pand".
    if (ref $pand eq 'ARRAY') {
        if (@$pand != 1) {
            $self->log->debug(sprintf(
                "Found %d panden, expected 1 for: %s",
                scalar @$pand,
                $self->nummeraanduiding_id,
            ));
        }
        return if @$pand == 0;

        $pand = $pand->[0];
    }

    my %rv = _extract_common_fields($pand);
    $rv{year_of_construction} = $pand->{bouwjaar};

    return \%rv;
}

=head2 pand_as_string

Always returns an empty string ('')

=cut

sub pand_as_string {
    return '';
}

=head2 ligplaats

Returns a hash reference with information about the "Ligplaats" part of the
BAG object. If the object does not contain a ligplaats, nothing is returned.

Contains the common keys (see below).

=cut

sub ligplaats {
    my $self = shift;
    return unless exists $self->bag_object->{woonplaats}{openbareruimte};
    return unless exists $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding};
    return unless exists $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}{ligplaats};

    my $lig = $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}{ligplaats};
    my %rv = _extract_common_fields($lig);

    return \%rv;
}

=head2 ligplaats_as_string

Always returns '-'

=cut

sub ligplaats_as_string {
    return '-';
}

=head2 standplaats

Returns a hash reference with information about the "Standplaats" part of the
BAG object. If the object does not contain a standplaats, nothing is returned.

Contains the common keys (see below).

=cut

sub standplaats {
    my $self = shift;
    return unless exists $self->bag_object->{woonplaats}{openbareruimte};
    return unless exists $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding};
    return unless exists $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}{standplaats};

    my $sta = $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}{standplaats};

    my %rv = _extract_common_fields($sta);

    return \%rv;
}

=head2 standplaats_as_string

Always returns '-'

=cut

sub standplaats_as_string {
    return '-';
}

=head2 get_identifiers

Returns a hash reference containing all the BAG identifiers, suitable for creating
a C<ZaakBag> row.

=cut

sub get_identifiers {
    my $self = shift;
    my $bag_object = $self->bag_object;

    my %rv = (
        bag_openbareruimte_id => $bag_object->{woonplaats}{openbareruimte}{id},
        bag_coordinates_wsg   => $bag_object->{geo_hash},
    );

    if (exists $bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}) {
        my $nummeraanduiding = $bag_object->{woonplaats}{openbareruimte}{nummeraanduiding};

        $rv{bag_nummeraanduiding_id} = $nummeraanduiding->{id};

        if (exists $nummeraanduiding->{verblijfsobject}) {
            $rv{bag_verblijfsobject_id} = $nummeraanduiding->{verblijfsobject}{id};

            # While a "verblijfsobject" can consist of multiple "pand" records,
            # we only store one of them in ZaakBag for now
            if(exists $nummeraanduiding->{verblijfsobject}{pand}) {
                if (ref $nummeraanduiding->{verblijfsobject}{pand} eq 'ARRAY') {
                    $rv{bag_pand_id} = $nummeraanduiding->{verblijfsobject}{pand}[0]{id};
                }
                else {
                    $rv{bag_pand_id} = $nummeraanduiding->{verblijfsobject}{pand}{id};
                }
            }
        }

        $rv{bag_ligplaats_id} = $bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}{ligplaats}{id}
            if (exists $bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}{ligplaats});
        $rv{bag_standplaats_id} = $bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}{standplaats}{id}
            if (exists $bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}{standplaats});
    }

    return \%rv;
}

=head2 to_attribute_value

Returns the value as stored in Case object attributes.

=cut

sub to_attribute_value {
    my $self = shift;
    my $bag_type = shift;

    my $bag_data = $self->$bag_type;
    return unless $bag_data;

    my $to_string_method = "${bag_type}_as_string";

    return {
        bag_id           => sprintf("%s-%s", $bag_type, $bag_data->{identification}),
        human_identifier => $self->$to_string_method(),
        address_data     => $bag_data,
    };
}

=head2 to_string

Returns a human-readable form of the address represented by the BAG object.

=cut

sub to_string {
    my $self = shift;

    return $self->nummeraanduiding_as_string if exists $self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding};
    return $self->openbareruimte_as_string   if exists $self->bag_object->{woonplaats}{openbareruimte};
    return $self->woonplaats_as_string       if exists $self->bag_object->{woonplaats};

    return '';
}

=head2 to_maps_result

Returns the value in a way the "plugins/maps" API endpoint understands.

=cut

sub to_maps_result {
    my $self = shift;

    my ($latitude, $longitude) = split(/,/, $self->bag_object->{geo_lat_lon});

    my $huisnummer = _build_huisnummer($self->bag_object->{woonplaats}{openbareruimte}{nummeraanduiding});

    my $address = sprintf(
        "%s %s, %s %s, %s",
        $self->bag_object->{straatnaam},
        $huisnummer,
        $self->bag_object->{postcode},
        $self->bag_object->{plaats},
        $self->bag_object->{land}
    );

    return {
       country  => $self->bag_object->{land},
       city     => $self->bag_object->{plaats},
       street   => $self->bag_object->{straatnaam},
       zipcode  => $self->bag_object->{postcode},
       province => $self->bag_object->{provincie},
       coordinates => {
           lat => $latitude,
           lng => $longitude,
       },
       identification => $address,
       address => $address,
    }
}

=head2 TO_JSON

JSON representation, for the "JSONlegacy" view used by ObjectSearch.

=cut

sub TO_JSON {
    my $self = shift;

    my $bag_object = $self->bag_object;

    if (exists $bag_object->{woonplaats}{openbareruimte}{nummeraanduiding}) {
        my $nummeraanduiding_id = $self->nummeraanduiding_id;

        my $huisnummer = _build_huisnummer($bag_object->{woonplaats}{openbareruimte}{nummeraanduiding});

        return {
            id => $nummeraanduiding_id,
            object_type => 'bag_address',
            label  => $self->nummeraanduiding_as_string,
            object => {
                id          => $nummeraanduiding_id,
                type        => 'address',
                number      => $huisnummer,
                street      => $bag_object->{straatnaam},
                postal_code => $bag_object->{postcode},
                city        => $bag_object->{plaats},
            },
        };
    }
    else {
        my $openbareruimte_id = sprintf(
            'openbareruimte-%s',
            $self->bag_object->{woonplaats}{openbareruimte}{id}
        );

        return {
            id          => $openbareruimte_id,
            object_type => 'bag_street',
            label       => $self->openbareruimte_as_string,
            object      => {
                id         => $openbareruimte_id,
                type       => 'street',
                streetname => $bag_object->{straatnaam},
                city       => $bag_object->{plaats},
            }
        }
    }
}

=head2 _build_huisnummer

Build up the "full" house number from parts (huisnummer, huisletter, huisnummer_toevoeging).

=cut

sub _build_huisnummer {
    my $nummeraanduiding = shift;

    my $huisnummer = $nummeraanduiding->{huisnummer};
    my $huisletter = $nummeraanduiding->{huisletter};
    my $huisnummer_toevoeging = $nummeraanduiding->{huisnummer_toevoeging};

    my $hn = $huisnummer;
    $hn .= $huisletter
        if defined $huisletter && length($huisletter);
    $hn .= "-$huisnummer_toevoeging"
        if defined $huisnummer_toevoeging && length($huisnummer_toevoeging);

    return $hn;
}

=head2 _extract_common_fields

Extracts the common fields from a part of the BAG object.

The common fields are:

=over

=item * start_date

=item * end_date

=item * document_date

=item * document_number

=item * under_investigation

=back

=cut

{
    my %COMMON_FIELDS = (
        begindatum     => 'start_date',
        einddatum      => 'end_date',
        documentdatum  => 'document_date',
        documentnummer => 'document_number',
        in_onderzoek   => 'under_investigation',
        id             => 'identification',
    );

    sub _extract_common_fields {
        my $object = shift;

        my %rv;
        for my $key (keys %COMMON_FIELDS) {
            my $return_key = $COMMON_FIELDS{$key};
            $rv{$return_key} = $object->{$key};
        }

        return %rv;
    }
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
