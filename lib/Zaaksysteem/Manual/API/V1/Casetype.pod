=head1 NAME

Zaaksysteem::Manual::API::V1::Casetype - Casetype retrieval API. 

=head1 Description

This API-document describes the usage of our JSON Casetype API. Via the Casetype API it is possible
to retrieve a list of single casetype, which describes the different phases, attributes, etc.

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 URL

The base URL for this API is:

    /api/v1/casetype

Make sure you use the HTTP Method C<GET> for retrieving, and C<POST> for change requests.

=head1 Retrieve data

=head2 get

   /api/v1/casetype/d7c73f04-5fcc-4c03-b089-b5fc760a08d5

Retrieving an object from our database is as simple as calling the URL C</api/v1/casetype/[UUID]>. You will get
the response in the C<result> parameter. It will only contain one single result, and as always, the C<type>
will tell you what kind of object you received. The C<instance> property will contain the contents of this
object.

Inside the C<result> property, you will find an object of C<type>: "casetype". This object contains information
about a casetype, like which attributes can be used for creating a case of this type, which results can be set,
etc.

For more information about this object, please look at the reference at the end of this document.

B<Example call>

  curl --anyauth -H "Content-Type: application/json" --digest -u "username:password" https://localhost/api/v1/casetype/d7c73f04-5fcc-4c03-b089-b5fc760a08d5

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-6376c8-0be0de",
   "development" : false,
   "result" : {
      "instance" : {
         "id" : "1af04983-b7c5-4eef-95ca-960d501f8f99",
         "legacy" : {
            "zaaktype_id" : 51,
            "zaaktype_node_id" : 351
         },
         "phases" : [
            {
               "fields" : [
                  {
                     "catalogue_id" : 301,
                     "help" : "Vul de benodigde gegevens in",
                     "id" : 301,
                     "is_group" : true,
                     "is_system" : false,
                     "label" : "Benodigde gegevens",
                     "magic_string" : "KSqp4DDyz8",
                     "multiple_values" : false,
                     "permissions" : [],
                     "required" : false,
                     "type" : "text",
                     "referential" : false,
                     "values" : []
                  },
                  {
                     "catalogue_id" : 302,
                     "help" : "EFss5JTur8",
                     "id" : "302",
                     "is_group" : false,
                     "is_system" : false,
                     "label" : "XLvs2ZZyz8",
                     "magic_string" : "checkbox_agreed",
                     "multiple_values" : true,
                     "permissions" : [],
                     "required" : false,
                     "referential" : false,
                     "type" : "checkbox",
                     "values" : [
                        {
                           "active" : 1,
                           "id" : 51,
                           "sort_order" : 51,
                           "value" : "agreed"
                        }
                     ]
                  },
                  {
                     "catalogue_id" : 303,
                     "help" : "ZLrc2EZkz4",
                     "id" : "303",
                     "is_group" : false,
                     "is_system" : false,
                     "label" : "BMnk9FYeg3",
                     "magic_string" : "test_comment",
                     "multiple_values" : false,
                     "permissions" : [],
                     "required" : false,
                     "type" : "text",
                     "referential" : false,
                     "values" : []
                  },
                  {
                     "catalogue_id" : 305,
                     "help" : "XOos2UWjf5",
                     "id" : "305",
                     "is_group" : false,
                     "is_system" : false,
                     "label" : "BMkz1PFsb7",
                     "magic_string" : "test_paspoort",
                     "multiple_values" : false,
                     "permissions" : [],
                     "required" : false,
                     "type" : "file",
                     "referential" : false,
                     "values" : []
                  }
               ],
               "id" : 101,
               "locked" : true,
               "name" : "registratiefase",
               "seq" : 1
            },
            {
               "fields" : [
                  {
                     "catalogue_id" : 304,
                     "help" : "QRjl0BJbz4",
                     "id" : 304,
                     "is_group" : false,
                     "is_system" : false,
                     "label" : "DAok6CPqt4",
                     "magic_string" : "test_conclusion",
                     "multiple_values" : false,
                     "permissions" : [],
                     "required" : false,
                     "type" : "text",
                     "referential" : false,
                     "values" : []
                  },
                  {
                     "catalogue_id" : 306,
                     "help" : "GWuh8WYcf4",
                     "id" : "306",
                     "is_group" : false,
                     "is_system" : false,
                     "label" : "OWms0CZfh2",
                     "magic_string" : "test_decision",
                     "multiple_values" : false,
                     "permissions" : [],
                     "required" : false,
                     "type" : "file",
                     "referential" : false,
                     "values" : []
                  }
               ],
               "id" : 102,
               "locked" : false,
               "name" : "afhandelfase",
               "seq" : 2
            }
         ],
         "preset_client" : "betrokkene-bedrijf-284",
         "results" : [
            {
               "id" : 102,
               "label" : "PHmy2JVuj6",
               "type" : "afgebroken"
            },
            {
               "id" : 101,
               "label" : "PUvt9HRkj1",
               "type" : "verwerkt"
            }
         ],
         "sources" : [
            "behandelaar",
            "balie",
            "telefoon",
            "post",
            "email",
            "webformulier",
            "sociale media"
         ],
         "subject_types" : [
            "preset_client"
         ],
         "title" : "QClr5OUsi0",
         "trigger" : "extern"
      },
      "reference" : "1af04983-b7c5-4eef-95ca-960d501f8f99",
      "type" : "casetype"
   },
   "status_code" : 200
}

=end javascript

Retrieving multiple objects from our database is as simple as calling the URL C</api/v1/case> without arguments. The
C<result> property will contain an object of type B<set>, which allows us to page the data.

B<Example call>

 curl --anyauth -H "Content-Type: application/json" --digest -u "username:password" https://localhost/api/v1/casetype

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-6376c8-3188fd",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 4,
            "total_rows" : 4
         },
         "rows" : [
            {
               "instance" : {
                  "id" : "d2a0f744-3617-4682-bccc-46cfcd672127",
                  "legacy" : {
                     "zaaktype_id" : 55,
                     "zaaktype_node_id" : 379
                  },
                  "phases" : [
                     {
                        "fields" : [
                           {
                              "catalogue_id" : 325,
                              "help" : "Vul de benodigde gegevens in",
                              "id" : 325,
                              "is_group" : true,
                              "is_system" : false,
                              "label" : "Benodigde gegevens",
                              "magic_string" : "GQnd8JYrq7",
                              "multiple_values" : false,
                              "permissions" : [],
                              "required" : false,
                              "type" : "text",
                              "values" : []
                           },
                           {
                              "catalogue_id" : 326,
                              "help" : "BQfj7BLib0",
                              "id" : "326",
                              "is_group" : false,
                              "is_system" : false,
                              "label" : "PJlc5CAap8",
                              "magic_string" : "checkbox_agreed",
                              "multiple_values" : true,
                              "permissions" : [],
                              "required" : false,
                              "type" : "checkbox",
                              "values" : [
                                 {
                                    "active" : 1,
                                    "id" : 55,
                                    "sort_order" : 55,
                                    "value" : "agreed"
                                 }
                              ]
                           },
                           {
                              "catalogue_id" : 327,
                              "help" : "XAal0BIuc9",
                              "id" : "327",
                              "is_group" : false,
                              "is_system" : false,
                              "label" : "RJph6LPto8",
                              "magic_string" : "test_comment",
                              "multiple_values" : false,
                              "permissions" : [],
                              "required" : false,
                              "type" : "text",
                              "values" : []
                           },
                           {
                              "catalogue_id" : 329,
                              "help" : "CXom9EJuz1",
                              "id" : "329",
                              "is_group" : false,
                              "is_system" : false,
                              "label" : "KBpr9SSsk3",
                              "magic_string" : "test_paspoort",
                              "multiple_values" : false,
                              "permissions" : [],
                              "required" : false,
                              "type" : "file",
                              "values" : []
                           }
                        ],
                        "id" : 109,
                        "locked" : true,
                        "name" : "registratiefase",
                        "seq" : 1
                     },
                     {
                        "fields" : [
                           {
                              "catalogue_id" : 328,
                              "help" : "FLzz0BCct2",
                              "id" : 328,
                              "is_group" : false,
                              "is_system" : false,
                              "label" : "SPml2XLkx6",
                              "magic_string" : "test_conclusion",
                              "multiple_values" : false,
                              "permissions" : [],
                              "required" : false,
                              "type" : "text",
                              "values" : []
                           },
                           {
                              "catalogue_id" : 330,
                              "help" : "RVgm5SSfg0",
                              "id" : "330",
                              "is_group" : false,
                              "is_system" : false,
                              "label" : "ORem1ZJol3",
                              "magic_string" : "test_decision",
                              "multiple_values" : false,
                              "permissions" : [],
                              "required" : false,
                              "type" : "file",
                              "values" : []
                           }
                        ],
                        "id" : 110,
                        "locked" : false,
                        "name" : "afhandelfase",
                        "seq" : 2
                     }
                  ],
                  "preset_client" : null,
                  "results" : [
                     {
                        "id" : 110,
                        "label" : "DLod1FDxv4",
                        "type" : "afgebroken"
                     },
                     {
                        "id" : 109,
                        "label" : "TUkl6CYdo2",
                        "type" : "verwerkt"
                     }
                  ],
                  "sources" : [
                     "behandelaar",
                     "balie",
                     "telefoon",
                     "post",
                     "email",
                     "webformulier",
                     "sociale media"
                  ],
                  "subject_types" : [
                     "natuurlijk_persoon",
                     "niet_natuurlijk_persoon"
                  ],
                  "title" : "WQhj2BBzn4",
                  "trigger" : "extern"
               },
               "reference" : "d2a0f744-3617-4682-bccc-46cfcd672127",
               "type" : "casetype"
            },
            { ... },
            { ... }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}


=end javascript

=head1 Objects

=head2 Casetype object properties

The returned casetype object describes the essential data of a casetype. Our goal is to supply all the
necessary data to help you create a new L<Zaaksysteem::Manual::API::V1::Case>.

We will start with describing the toplevel properties, and will go deeper into the related objects.

=head3 id

B<TYPE>: UUID

This property contains the unique identifier of this casetype, use it when creating a new case via the case API

=head3 title

B<TYPE>: Str

The human defined title for this casetype, like "Parkeervergunning"

=head3 legacy

B<TYPE>: Object

Legacy properties. Contains properties for the purpose of transitioning from an old to a new API.

B<Please: Do not use these data unless necessary, these will disappear without warning in the near future>

=head3 preset_client

B<TYPE>: Subject

The subject which will be set when no requestor is given at case creation.

=begin javascript

{
    ...,
    "preset_client": {
        "type": "subject",
        "reference": "198ce2d2-6400-46b1-ac91-e58b28dd6312",
        "instance": {
            ...
        }
    }
}

=end javascript

=head3 trigger

B<Type>: ENUM (extern | intern | externintern)

=head3 subject_types

B<Type>: List of possible subject types (natuurlijk_persoon | niet_natuurlijk_persoon | medewerker)

=head3 sources

A list of sources you could use to register a new case of this type.

=begin javascript

"sources" : [
   "behandelaar",
   "balie",
   "telefoon",
   "post",
   "email",
   "webformulier"
]

=end javascript

=head3 results

B<TYPE>: Object

Contains a list of possible results ("resultaten") for this casetype. When using the case API, you would
use the id for the C<result_id> attribute in the transition call.

=begin javascript

"results" : [
   {
      "id" : 757,
      "label" : "Afgebroken",
      "type" : "afgebroken"
   },
   {
      "id" : 756,
      "label" : "Verwerkt",
      "type" : "verwerkt"
   }
]

=end javascript

=head3 phases

B<TYPE>: Array

A list of phases, containing the fields which you can use when you create a case via our API. Use the C<id>
property of a field when creating a with this value.

=head4 fields

Fields indicate that the casetype has several attributes and/or fields.

=over

=item id

The id of the attribute

=item catalogue_id

The internal catalogue id

=item label

The label of the attribute

=item help

The help message displayed to the user

=item is_group

A boolean value which says the attribute is a group

=item is_system

A boolean value which says the attribute is a system attribute (not editable by the user).

=item magic_string

The magic string of the attribute

=item multiple_values

A boolean value to indicate if the attribute can hold multiple values.
Please note that when you update a value, which is always in list context and the attribute does not
allow multiple values, only the first item is used for the attribute.

=item permissions

Indicates which group and role have permission to update the attribute.
An empty list indicates that everyone can update the attribute
A list where the group and/or role ID's are set indicate that users from
that group and/or role may update the attribute. If a group and/or role has an
ID of 0, this means it has been deleted from the database. An additional invalid
flag has been set as well.

=item required

A boolean value to indicate the attribute is required for a phase transitioning.

=item type

The type of an attribute.

=item referential

A boolean value to indicate the attribute is referential.
This means that the value cannot be updated via the current case, but only by its parent.

=item values

The values in list context.

=back

=begin javascript

"phases" : [
   {
      "fields" : [
         {
            "catalogue_id" : 247,
            "help" : "Vul de benodigde gegevens in",
            "id" : 247,
            "is_group" : true,
            "label" : "Benodigde gegevens",
            "magic_string" : "OHkm7XCdo9",
            "multiple_values" : false,
            "permissions" : [],
            "required" : false,
            "type" : "text",
            "values" : []
         },
         {
            "catalogue_id" : 248,
            "help" : "FMvl5AOmh3",
            "id" : "248",
            "is_group" : false,
            "label" : "PDkr6OJab9",
            "magic_string" : "checkbox_agreed",
            "multiple_values" : true,
            "permissions" : [],
            "required" : false,
            "type" : "checkbox",
            "values" : [
               {
                  "active" : 1,
                  "id" : 42,
                  "sort_order" : 42,
                  "value" : "agreed"
               }
            ]
         }
      ],
      "id" : 83,
      "locked" : true,
      "name" : "registratiefase",
      "seq" : 1
   },
   {
      "fields" : [
         {
            "catalogue_id" : 250,
            "help" : "MLnp0RZdp5",
            "id" : 250,
            "is_group" : false,
            "label" : "VSmi0CTlt6",
            "magic_string" : "test_conclusion",
            "multiple_values" : false,
            "permissions" : [],
            "required" : false,
            "type" : "text",
            "values" : []
         },
         {
            "catalogue_id" : 252,
            "help" : "RByz6GEbd1",
            "id" : "252",
            "is_group" : false,
            "label" : "ZPtt1QEih3",
            "magic_string" : "test_decision",
            "multiple_values" : false,
            "permissions" : [],
            "required" : false,
            "type" : "file",
            "values" : []
         }
      ],
      "id" : 84,
      "locked" : false,
      "name" : "afhandelfase",
      "seq" : 2
   }
],

=end javascript

=head2 list_allowed_users

   /api/v1/casetype/list_allowed_users/[casetype:uuid]

Retrieve allowed users per casetype

B<Example call>

  curl --anyauth -H "Content-Type: application/json" --digest -u "username:password" https://localhost/api/v1/casetype/list_allowed_users

B<Example call>

  curl --anyauth -H "Content-Type: application/json" --digest -u "username:password" https://localhost/api/v1/casetype/list_allowed_users/d7c73f04-5fcc-4c03-b089-b5fc760a08d5

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "development" : true,
   "request_id" : "mintlab_sprint-a7836a-7f7e92",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : "https://sprint.zaaksysteem.nl/api/v1/casetype/list_allowed_users?page=2",
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 1,
            "total_rows" : 1
         },
         "rows" : [
            {
               "instance" : {
                  "casetype_active" : true,
                  "casetype_uuid" : "6f17bd29-d88c-4902-b9ae-d638c2b0d1b6",
                  "date_created" : "2016-09-30T12:55:01Z",
                  "date_modified" : "2016-09-30T12:55:01Z",
                  "public" : {
                     "read" : [
                        "subafdelingb",
                        "subafdelinga",
                        "rechten",
                        "user5"
                     ]
                  },
                  "trusted" : {
                     "read" : []
                  }
               },
               "reference" : null,
               "type" : "casetype_acl"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::Controller::API::V1::CaseType>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
