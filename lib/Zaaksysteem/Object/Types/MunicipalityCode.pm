package Zaaksysteem::Object::Types::MunicipalityCode;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

use BTTW::Tools;
use List::Util qw(first);
use Zaaksysteem::Object::ConstantTables qw/MUNICIPALITY_TABLE/;
use Zaaksysteem::Types qw(NonEmptyStr MunicipalityCode);

=head1 NAME

Zaaksysteem::Object::Types::MunicipalityCode - Built-in object type
implementing a class for MunicipalityCode objects

=head1 DESCRIPTION

An object class for municipality codes. This module implements codes for dutch
municipalities

=head1 ATTRIBUTES

=head2 dutch_code

The Dutch government uses their own codes, these are known as RGBZ gemeente
codes.

=cut

has dutch_code => (
    is       => 'rw',
    isa      => MunicipalityCode,
    coerce   => 1,
    traits   => [qw(OA)],
    label    => 'Dutch municipality code',
    required => 1,
    unique   => 1,
);

=head2 label

The name of the municipality

=cut

has label => (
    is       => 'rw',
    isa      => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'Name of the municipality',
    required => 1,
);

=head2 alternative_name

Some municipalities have an alternative name, this describes that name

=cut

has alternative_name => (
    is        => 'rw',
    isa       => NonEmptyStr,
    traits    => [qw(OA)],
    label     => 'Alternative name of the municipality',
    predicate => 'has_alternative_name',
);

=head1 METHODS

=head2 new_from_dutch_code

    $municipality_code = Zaaksysteem::Object::Types::MunicipalityCode
        ->new_from_dutch_code(3);

Loads object from dutch_code

=cut

sig new_from_dutch_code => 'Int';

sub new_from_dutch_code {
    my ($class, $dutch_code) = @_;

    my $municipality_code = first { $_->{dutch_code} eq int($dutch_code) } @{ MUNICIPALITY_TABLE() };

    return $class->new(%$municipality_code) if $municipality_code;

    throw(
        'object/types/municipalitycode/unknown_dutch_code',
        sprintf('Unknown code no municipality by dutch_code: "%s"',
            $dutch_code,
        )
    );

}

=head2 new_from_dutch_code

    $municipality_code = Zaaksysteem::Object::Types::MunicipalityCode
        ->new_from_name(3);

Loads object from name, case-insensitve

=cut


sig new_from_name => 'Str';

sub new_from_name {
    my ($class, $name) = @_;

    my $municipality_code = first { lc($_->{label}) eq lc($name) } @{ MUNICIPALITY_TABLE() };

    return $class->new(%$municipality_code) if $municipality_code;

    throw(
        'object/types/municipalitycode/unknown_name',
        sprintf('Unknown name no municipality by name: "%s"',
            $name,
        )
    );

}

sub new_from_uuid {
    my ($class, $uuid) = @_;
    my $municipality_code = first { lc($_->{id}) eq $uuid } @{ MUNICIPALITY_TABLE() };

    return $class->new(%$municipality_code) if $municipality_code;

    throw(
        'object/types/municipalitycode/unknown_name',
        sprintf('Unknown ID: No municipality by UUID "%s"',
            $uuid,
        )
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, 2018 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
