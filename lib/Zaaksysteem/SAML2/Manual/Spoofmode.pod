=head1 SAML - Spoofmode

=head2 Identity Provider Interface

This feature, at least for now, requires the full configuration of some
Identify Provider in the managed interfaces section of Zaaksysteem. Every
setting should be setup as one would a normal mode SAML Protocol Exchange,
with the exception of the C<SAML Implementatie> field, which should be set
to C<Spoofmode>.

Additionally, this setup will only function when the configuration file
for this instance of Zaaksysteem (see L<Zaaksysteem::Manual::Config>) has a
key C<dev> set to a true-ish value. This cannot be set on the C<customer.d>
configuration file, it is global for the instance and all it's customers.

Currently, the Zaaksysteem instances on the development server have this bit
set (see L<Zaaksysteem::Manual::Servers>).

=head3 Example

=for html <img src="/images/manual/saml/config-idp-spoofmode.png" />

=head2 Usage

The usage of the spoofmode follows the same steps as the usage for any other
SAML Protocol Exchange. Head to either C</pip> or C</form>, and follow the
authentication process. At one point, instead of a SAML Authentication UI,
you will find this:

=for html <img src="/images/manual/saml/spoof-mode-example.png" />

Depending on whether you want to login as an organisation or as a citizen, you
enter the C<BSN> or C<KvK> entity identifier, and hit the 'Spoof' button. If
all goes as it should, you will be logged in as that entity. Beware that you
can enter arbitrary numbers here, and the error checking on this module isn't
great, so some fiddeling might be required.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
