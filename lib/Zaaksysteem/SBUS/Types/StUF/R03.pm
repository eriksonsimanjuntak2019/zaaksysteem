package Zaaksysteem::SBUS::Types::StUF::R03;

use Data::Dumper;
use Moose;

use Zaaksysteem::SBUS::Constants;

extends 'Zaaksysteem::SBUS::Types::StUF::GenericAdapter';

sub _stuf_to_params {
    my ($self, $prs_xml, $stuf_options) = @_;

    return $self->_convert_stuf_to_hash(
        $prs_xml,
        {
            ingangsdatum    => 'begindatum',
            einddatum       => 'einddatum',
        }
    );
}

sub _stuf_relaties {
    my ($self, $prs_xml, $stuf_options, $create_options) = @_;

    ### Handle real data
    return unless $prs_xml->{extraElementen};

    my $bag         = $self->_convert_stuf_extra_elementen_to_hash(
        $prs_xml->{extraElementen}
    );

    my $mapping     = {
        identificatieWoonplaats => 'identificatie',
        woonplaatsNaamBAG       => 'naam',
        status                  => 'status',
    };

    my $bag_data    = $self->_convert_stuf_to_hash(
        $bag,
        $mapping
    );

    $create_options->{ $_ } = $bag_data->{ $_ }
        for keys %{ $bag_data };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
