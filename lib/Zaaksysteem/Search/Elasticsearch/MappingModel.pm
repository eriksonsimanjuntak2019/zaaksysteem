package Zaaksysteem::Search::Elasticsearch::MappingModel;

use Moose;
use namespace::autoclean;

with qw[
    MooseX::Log::Log4perl
];

=head1 NAME

Zaaksysteem::Search::Elasticsearch::MappingModel - Zaaksysteem attribute-mapping
model

=head1 DESCRIPTION

This model abstracts interactions with an Elasticsearch index given mutations
on L<Zaaksysteem::DB::Component::BibliotheekKenmerken> objects.

This package is a thin shim over Elasticsearch for ease of testing, but is not
expected to be re-used.

=head1 SYNOPSIS

    my $model = $c->model('ESAIM');

    $model->initialize_index;

=cut

use BTTW::Tools;
use Moose::Util::TypeConstraints qw[role_type];
use Syzygy::Object::Model;
use Zaaksysteem::Search::Elasticsearch::Constants qw[
    SZG_VALUE_TYPE_MAPPING_MAP
    ZS_VALUE_TYPE_MAPPING_MAP
];

=head1 ATTRIBUTES

=head2 elasticsearch

Reference to a L<Search::Elasticsearch::Role::Client>-implementing object.

=head3 Delegates

=over 4

=item _indices

Calls C<indices> on the client.

=back

=cut

has elasticsearch => (
    is => 'rw',
    isa => role_type('Search::Elasticsearch::Role::Client'),
    required => 1,
    handles => {
        _indices => 'indices'
    }
);

=head2 index_name

String name of the index the model should interact with.

=cut

has index_name => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head2 _szg_value_type_mapping_map

Convenience attribute for the
L<Zaaksysteem::Search::Elasticsearch::Constants/SZG_VALUE_TYPE_MAPPING>
constant.

=head3 Delegates

=over 4

=item has_szg_value_type_mapping

Given a Syzygy value type name, returns a boolean value indicating the
existence of a mapping for the type.

=item get_szg_value_type_mapping

Given a Syzygy value type name, returns the Elasticsearch mapping field
specification.

=back

=cut

has _szg_value_type_mapping_map => (
    is => 'ro',
    isa => 'HashRef',
    traits => [qw[Hash]],
    default => sub { return SZG_VALUE_TYPE_MAPPING_MAP() },
    handles => {
        has_szg_value_type_mapping => 'exists',
        get_szg_value_type_mapping => 'get'
    }
);

=head2 _zs_value_type_mapping_map

Convenience attribute for the
L<Zaaksysteem::Search::Elasticsearch::Constants/ZS_VALUE_TYPE_MAPPING>
constant.

=head3 Delegates

=over 4

=item has_zs_value_type_mapping

Given a value type name of a 'classic' Zaaksysteem attribute, returns a
boolean value indicating the existence of a mapping for the type.

=item get_zs_value_type_mapping

Given a value type name, returns the Elasticsearch mapping field
specification.

=back

=cut

has _zs_value_type_mapping_map => (
    is => 'ro',
    isa => 'HashRef',
    traits => [qw[Hash]],
    default => sub { return ZS_VALUE_TYPE_MAPPING_MAP() },
    handles => {
        has_zs_value_type_mapping => 'exists',
        get_zs_value_type_mapping => 'get'
    }
);

=head1 METHODS

=head2 initialize

This methods creates a new index in the Elasticsearch backend, unless one
already exists.

    if ($model->initialize) {
        # Elasticsearch's primary shards acknowledge the new index
    } else {
        # Elasticsearch timed out on index creation, check later
    }

=head3 options

=over 4

=item force

Forces initialization by removing an existing index if one exist.

=back

=cut

define_profile initialize => (
    optional => {
        force => 'Bool'
    },
    defaults => {
        force => 0
    }
);

sub initialize {
    my ($self, %args) = @_;

    my $opts = assert_profile(\%args)->valid;

    if ($self->_exists) {
        return unless $opts->{ force };

        $self->_delete;
    }

    return $self->_create;
}

=head2 update_mapping

Update the index attribute mapping for a given set of
L<Zaaksysteem::DB::Component::BibliotheekKenmerken>.

=cut

sig update_mapping => '@Zaaksysteem::DB::Component::BibliotheekKenmerken';

sub update_mapping {
    my ($self, @kenmerken) = @_;

    # create-if-not-exists
    $self->initialize(force => 0);

    my %field_mappings = map { $self->build_zs_mapping_spec($_) } @kenmerken;

    $self->log->trace(sprintf(
        'Updating index field mappings: %s',
        dump_terse(\%field_mappings)
    ));

    return $self->_indices->put_mapping(
        index => $self->index_name,
        type => 'object',
        body => {
            object => {
                dynamic => \0,
                properties => \%field_mappings
            }
        }
    );
}

=head2 base_mappings

Gathers all attributes from L<Syzygy::Object::Model>, filters for the
attributes in the C<sys> namespace, and builds the field mapping for the
filtered set.

=cut

sub base_mappings {
    my $self = shift;

    my $model = Syzygy::Object::Model->get_instance;

    # This is how we should get the list of core attributes when Syzygy is
    # up to spec
    # my %core_properties = map {
    #     $self->build_szg_mapping_spec($_)
    # } grep {
    #     $_->namespace eq 'core'
    # } $model->all_attributes;

    my $mappings = {
        # All objects have an 'rich' reference representation in the index
        object => {
            dynamic => \0,
            properties => {
                # These properties are shared by all objects
                'core.preview'      => { type => 'text' },
                'core.keywords'     => { type => 'text' },
                'core.ref.id'       => { type => 'keyword' },
                'core.ref.type'     => { type => 'keyword' },
                'core.intref.id'    => { type => 'keyword' },
                'core.intref.table' => { type => 'keyword' },
            }
        }
    };

    return $mappings;

    # Mapping injection for specific object types not used yet, for ease
    # of evaluating ES as a component of Zaaksysteem this bit is turned off
    # until we're actually going to use it.

    for my $object_type ($model->all_object_types) {
        my %properties = map {
            $self->build_szg_mapping_spec($_)
        } grep {
            # Probably overkill, but doesn't hurt to be safe in case we ever
            # allow user-defined extensions on core types.
            $_->namespace eq 'sys'
        } $object_type->all_attributes;

        $mappings->{ $object_type->name } = {
            dynamic => \0,
            properties => \%properties,
        };
    }

    return $mappings;
}

=head2 build_szg_mapping_spec

Given a L<Syzygy::Object::Attribute> instance, this method returns
the field mapping definition for Elasticsearch's mapping API.

    my ($field, $spec) = $esaim->build_szg_mapping_spec($my_attr);

=cut

sig build_szg_mapping_spec => 'Syzygy::Object::Attribute';

sub build_szg_mapping_spec {
    my $self = shift;
    my $attribute = shift;

    unless ($self->has_szg_value_type_mapping($attribute->value_type_name)) {
        throw('elasticsearch/attribute/value_type_unsupported', sprintf(
            'Value type "%s" unsupported for ES field type mapping',
            $attribute->value_type_name
        ));
    }

    my $name = sprintf('%s.%s', $attribute->namespace, $attribute->name);

    return $name => $self->get_szg_value_type_mapping($attribute->value_type_name);
}

=head2 build_zs_mapping_spec

Given a L<Zaaksysteem::DB::Component::BibliotheekKenmerken> instance, this
method returns the field mapping definition for Elasticsearch's mapping API.

    my ($field, $spec) = $esaim->build_zs_mapping_spec($my_kenmerk);

=cut

sig build_zs_mapping_spec => 'Zaaksysteem::DB::Component::BibliotheekKenmerken';

sub build_zs_mapping_spec {
    my $self = shift;
    my $kenmerk = shift;

    unless ($self->has_zs_value_type_mapping($kenmerk->value_type)) {
        throw('elasticsearch/attribute/value_type_unsupported', sprintf(
            'ZS Value type "%s" unsupported for ES field type mapping',
            $kenmerk->value_type
        ));
    }

    my $name = sprintf('usr.%s', $kenmerk->magic_string);

    return $name => $self->get_zs_value_type_mapping($kenmerk->value_type);
}

=head1 PRIVATE METHODS

=head2 _create

Creates the L</index_name> index, and adds the L</base_mapping> field mapping.

=cut

sub _create {
    my ($self, @passthru_args) = @_;

    $self->log->trace(sprintf('Creating indexes for instance "%s"', $self->index_name));

    my $mappings = $self->base_mappings;

    for my $type_name (keys %{ $mappings }) {
        $self->_indices->create(
            index => sprintf('%s_%s', $self->index_name, $type_name),
            body => { mappings => {
                $type_name => $mappings->{ $type_name } }
            },
            @passthru_args
        );
    }

    return 1;
}

=head2 _delete

Removes the indexes named by L</index_name> from the connected Elasticsearch
server.

=cut

sub _delete {
    my ($self, @passthru_args) = @_;

    $self->log->trace(sprintf(
        'Deleting indexes for instance "%s"',
        $self->index_name
    ));

    my $res = $self->_indices->delete(
        index => sprintf('%s_*', $self->index_name),
        @passthru_args
    );

    return;
}

=head2 _exists

Checks the index C<L</index_name>_object> exists on the server.

=cut

sub _exists {
    my ($self, @passthru_args) = @_;

    return $self->_indices->exists(
        index => sprintf('%s_object', $self->index_name),
        @passthru_args
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
