package Zaaksysteem::Transaction;
use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Transaction - Model for transactions (interface logs)

=head1 DESCRIPTION

This model handles interaction with transactions, transaction records and
mappnigs of transaction records to objects.

Note that this doesn't mean I<database> transactions. It's about transactions
between Zaaksysteem and other systems.

=head1 SYNOPSIS

    my $model = Zaaksysteem::Transaction->new(schema => $db, config => { ... });
    $model->clean_old_transactions();

    my $model = $c->model('Transaction');
    $model->clean_old_transactions()

=cut

my $DEFAULT_TRANSACTION_MAXIMUM_AGE_DAYS = 60;

=head1 ATTRIBUTES

=head2 schema

The database object (L<Zaaksysteem::Schema> instance) to use.

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

=head2 config

A hash reference, containing the contents of the "Transaction" section in the
Zaaksysteem configuration.

Recognised keys:

=over

=item * transaction_maximum_age_days

The maximum age of a transaction (in days) before it is deleted.

=back

=cut

has config => (
    is       => 'ro',
    isa      => 'HashRef',
    required => 1,
);

=head2 cleanup_old_transactions

Removes transactions older than the configured maximum age.

=cut

sub cleanup_old_transactions {
    my $self = shift;

    my $interval = sprintf(
        "(CURRENT_DATE - '%d days'::INTERVAL)",
        $self->config->{transaction_maximum_age_days} // $DEFAULT_TRANSACTION_MAXIMUM_AGE_DAYS
    );

    $self->schema->txn_do(sub {
        my $transactions = $self->schema->resultset('Transaction')->search(
            {
                'me.date_created::DATE' => { '<' => \$interval }
            }
        );
        my $transaction_records = $self->schema->resultset('TransactionRecord')->search(
            {
                transaction_id => { -in => $transactions->get_column('id')->as_query }
            }
        );
        my $transaction_record_objects = $self->schema->resultset('TransactionRecordToObject')->search(
            {
                transaction_record_id => { -in => $transaction_records->get_column('id')->as_query }
            }
        );

        $transaction_record_objects->delete;
        $transaction_records->delete;
        $transactions->delete;
    });

    return;
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
