package Zaaksysteem::ZTT::Selection::OpenOffice;

use Moose;

BEGIN { extends 'Zaaksysteem::ZTT::Selection' };

=head1 NAME

Zaaksysteem::ZTT::Selection::OpenOffice - Specific implementation of selections
for OpenOffice templates

=head1 ATTRIBUTES

=head2 element

This attribute holds a reference to an L<OpenOffice::OODoc::Element> object
that encapsulates the selected text in the template.

=cut

has element => (
    is => 'ro',
    isa => 'OpenOffice::OODoc::Element',
    required => 1
);

__PACKAGE__->meta->make_immutable;
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
