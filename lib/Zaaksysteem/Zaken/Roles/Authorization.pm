package Zaaksysteem::Zaken::Roles::Authorization;

use Moose::Role;

=head1 NAME

Zaaksysteem::Zaken::Roles::Authorization - Permission check infrastructure

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use List::Util qw[any];
use Zaaksysteem::Constants qw[
	ZAAKSYSTEEM_CASE_ACTION_AUTHORIZATION_MATRIX
];

=head1 METHODS

=head2 check_user_permissions

Checks that the given L<subject|Zaaksysteem::Backend::Subject::Component>
has the requested permissions.

    if ($zaak->check_user_permissions($c->user, 'zaak_read', ...)) {
        ...
    } else {
        barf;
    }

=cut

sig check_user_permissions => 'Zaaksysteem::Backend::Subject::Component, @Str';

sub check_user_permissions {
    my ($self, $subject, @permissions) = @_;

    $self->log->debug(sprintf(
        'Checking "%s" permissions for user "%s" (%s) on case %s',
        join(',', @permissions),
        $subject->display_name,
        $subject->uuid,
        $self->id
    ));

    return 1 if $subject->has_legacy_permission('admin');

    if (any { $_ eq 'zaak_read' || $_ eq 'zaak_search' } @permissions) {
        return 1 if $self->ddd_user_is_staff($subject);
    }

    if (any { $_ eq 'zaak_edit' || $_ eq 'zaak_search' } @permissions) {
        return 1 if $self->ddd_user_is_assignee($subject);
        return 1 if $self->ddd_has_assignee
                 && $self->ddd_user_is_coordinator($subject);
    }

    return 1 if $self->_check_user_casetype_permissions($subject, @permissions);
    return 1 if $self->_check_user_case_permissions($subject, @permissions);

    return 0;
}

sub _check_user_casetype_permissions {
    my ($self, $subject, @permissions) = @_;

    my $zta_rs = $self->zaaktype_id->zaaktype_authorisations->search({
        confidential => ($self->confidentiality eq 'confidential' ? 1 : 0),
        recht => \@permissions,
        ou_id => [ map { $_->id } @{ $subject->parent_groups } ],
        role_id => [ map { $_->id } @{ $subject->roles } ]
    });

    return $zta_rs->count ? 1 : 0;
}

sub _check_user_case_permissions {
    my ($self, $subject, @permissions) = @_;

    my %map = (
        zaak_read => 'read',
        zaak_search => 'search',
        zaak_edit => 'write',
        zaak_beheer => 'manage'
    );

    my @entity_ids;

    # NxM matrix of possible positions the user holds.
    for my $group_id (map { $_->id } @{ $subject->parent_groups }) {
        for my $role_id (map { $_->id } @{ $subject->roles }) {
            push @entity_ids, sprintf('%s|%s', $group_id, $role_id);
        }
    }

    my $za_rs = $self->zaak_authorisations->search({
        capability => [ map { $map{ $_ } } @permissions ],
        entity_id => \@entity_ids,
        entity_type => 'position',
        scope => 'instance',
    });

    return $za_rs->count ? 1 : 0;
}

sub _eval_case_action_authorization_matrix {
    my ($self, $user, $user_perm_checker, $context, $action) = @_;

    my $matrix = ZAAKSYSTEEM_CASE_ACTION_AUTHORIZATION_MATRIX;
    my $results = {};

    # First eval default context
    if ($context ne '_' and my $actions = $matrix->{ _ }) {
        my $_results = {};

        if (my $rules = $actions->{ _ }) {
            $_results->{ _ } = $self->_assert_auth_rules(
                $rules,
                $user,
                $user_perm_checker
            );
        }

        if (my $rules = $actions->{ $action }) {
            $_results->{ $action } = $self->_assert_auth_rules(
                $rules,
                $user,
                $user_perm_checker
            );
        }

        $results->{ _ } = $_results;
    }

    my $actions = $matrix->{ $context };
    my $_results = {};

    unless (defined $actions) {
        throw('zaak/authorization/context_does_not_exist', sprintf(
            'Requested case authorization context "%s" does not exist.',
            $context
        ));
    }

    if (my $rules = $actions->{ _ }) {
        $_results->{ _ } = $self->_assert_auth_rules(
            $rules,
            $user,
            $user_perm_checker
        );
    }

    my $rules = $actions->{ $action };

    unless (defined $rules) {
        throw('zaak/authorization/action_does_not_exist', sprintf(
            'Requested case authorization action "%s" does not exist in context "%s"',
            $action,
            $context
        ));
    }

    $_results->{ $action } = $self->_assert_auth_rules(
        $rules,
        $user,
        $user_perm_checker
    );

    $results->{ $context } = $_results;

    return $results;
}

sub _assert_auth_rules {
    my ($self, $rules, $user, $user_perm_checker) = @_;

	for my $rule (@{ $rules }) {
        if (my $scope = $rule->{ scope }) {
            next unless $user_perm_checker->($self, @{ $scope });
        }

        my $cond_fail;

        for my $condition (@{ $rule->{ conditions } }) {
            my $predicate = $self->can(sprintf('ddd_%s', $condition));

            $predicate //= $self->_get_auth_predicate($condition);

            unless (defined $predicate) {
                throw('zaak/authorization/predicate_does_not_exist', sprintf(
                    'Given authorization rule condition "%s" does not have a predicate',
                    $condition
                ));
            }

            next if $predicate->($self, $user);

            $cond_fail = 1;

            # Short-circuit rest of tests
            last;
        }

        next if $cond_fail;

        return sprintf(
            'in scope(%s), conditions(%s)',
            join(',', @{ $rule->{ scope } || [] }),
            join(',', @{ $rule->{ conditions } || [] })
        );
    }

    throw('zaak/authorization/user_not_authorized', sprintf(
        'Authorization condition(s) failed for user "%s" on case %s',
        $user->display_name,
        $self->id
    ));
}

sub _get_auth_predicate {
    # Empty for now, this is the intended hook for predicates that
    # don't resolve to a 'ddd_my_predicte' method, or need some other logic
    # for its retrieval.
    #
    # Implementors of this hook should return a subref that takes a case and a
    # user object, and returns a bool-ish value.
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
