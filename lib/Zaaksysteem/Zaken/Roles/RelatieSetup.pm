package Zaaksysteem::Zaken::Roles::RelatieSetup;
use Moose::Role;

around '_create_zaak' => sub {
    my $orig            = shift;
    my $self            = shift;
    my ($opts)          = @_;

    my $zaak = $self->$orig(@_);

    if (!$opts->{relatie}) {
        return $zaak;
    }

    my $related_case = $self->result_source->schema->resultset('Zaak')->find($opts->{zaak_id});

    $zaak->set_relatie({
            relatie                  => $opts->{relatie},
            actie_kopieren_kenmerken => $opts->{actie_kopieren_kenmerken},
            relatie_zaak             => $related_case,
    });

    return $zaak;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
