[% USE JSON %]
[% USE Dumper %]

<div
    class="clearfix casetype-children"
    data-ng-controller="nl.mintlab.admin.casetype.ChildCaseTypeController"
    data-ng-init="caseTypeId='[% zaaktype_id %]'"
>
    <div class="casetype-children-explanation">Geef aan welke kindzaaktypen moeten worden bijgewerkt bij het publiceren van dit zaaktype.</div>
    <ul class="casetype-children-list">
        <li
            class="casetype-children-list-item"
            data-ng-class="{'casetype-children-list-item-inactive': !relation.import, 'casetype-children-list-item-collapsed': relation._collapsed  }"
            data-ng-repeat="relation in children"
        >
            <div
                class="casetype-children-list-item-title"
                data-ng-click="relation._collapsed=!relation._collapsed"
            >
                <div class="casetype-children-list-item-label">
                    <i class="casetype-children-list-item-label-icon icon-font-awesome icon"></i>
                    <[relation.casetype.title]>
                </div>
                <div class="casetype-children-list-item-import">
                    <label>
                        <input
                            type="checkbox"
                            data-ng-model="relation.import"
                            data-ng-click="onImportToggleClick($event)"
                        >
                    </label>
                </div>
                <div class="casetype-children-list-item-remove">
                    <button
                        type="button"
                        class="button casetype-children-list-item-options-remove button-smaller"
                        data-ng-click="removeRelation(relation, $event)"
                    >
                        <i class="icon icon-font-awesome icon-remove"></i>
                    </button>
                </div>
            </div>
            <div
                class="casetype-children-list-item-options-wrapper"
                data-ng-show="!relation._collapsed"
            >
                <ul class="casetype-children-list-item-options">
                    <li
                        class="casetype-children-list-item-option"
                        data-ng-repeat="option in options"
                    >
                        <label>
                            <input
                                type="checkbox"
                                data-ng-model="relation[option.value]"
                                data-ng-disabled="!relation.import&&option.value!='import'"
                            >
                            <[option.label]>
                        </label>
                    </li>
                    <li class="casetype-children-list-item-option">
                        <label
                            data-ng-controller="nl.mintlab.admin.casetype.MiddlePhasesController"
                        >
                           <input
                               type="checkbox"
                               data-ng-model="relation.middle_phases"
                               data-ng-disabled="!relation.import"
                               data-zs-confirm
                               data-zs-confirm-enabled="relation.middle_phases"
                               data-zs-confirm-verb="Bevestigen"
                               data-zs-confirm-label="Weet u zeker dat u ook overige fasen wilt overschrijven met alle instellingen van het moederzaaktype?"
                           >
                            Overige fasen (inclusief toewijzing)
                         </label>
                    </li>
                </ul>
            </div>
        </li>
    </ul>

    <div class="child-casetype-add">
        <input
            type="text"
            data-ng-model="newCaseType"
            data-zs-placeholder="'Zoek een zaaktype om toe te voegen als kindzaaktype'"
            data-zs-spot-enlighter data-zs-spot-enlighter-restrict="'casetype'"
            data-zs-spot-enlighter-label="searchable_object_label"
        >
    </div>
</div>

<script type="text/zs-scope-data">
    {
        "children": [% JSON.encode(child_casetypes) %],
        "options": [% JSON.encode(ZCONSTANTS.CHILD_CASETYPE_OPTIONS) %]
    }
</script>
