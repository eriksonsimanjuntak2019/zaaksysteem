package TestFor::Catalyst::Controller::API::V1::Controlpanel;
use base qw(ZSTest::Catalyst);

use Moose;
use TestSetup;

=head1 NAME

TestFor::Catalyst::Controller:API::V1::Controlpanel - Proves the boundaries of our API: Controlpanel

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/Controlpanel.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/controlpanel> namespace.

=head1 USAGE

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 Create

=head3 create controlpanel

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/create

B<Request>

=begin javascript

{
   "customer_type" : "government",
   "owner" : "betrokkene-bedrijf-91"
}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-81b8d1-0134cc",
   "result" : {
      "instance" : {
         "customer_type" : "government",
         "id" : "c25d2daf-0e00-4fa5-8bc7-b61fff072234",
         "ipaddresses" : [],
         "owner" : "betrokkene-bedrijf-131",
         "template" : "mintlab"
      },
      "reference" : "c25d2daf-0e00-4fa5-8bc7-b61fff072234",
      "type" : "controlpanel"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_controlpanel_create : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        $self->_prepare_controlpanel;
        my $mech     = $zs->mech;
        my $company  = $zs->create_bedrijf_ok;

        $mech->zs_login;
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/create',
            {
                owner            => 'betrokkene-bedrijf-' . $company->id,
                customer_type    => 'government',
            }
        );

        my $json     = $mech->content;
        my $cp_data  = $self->_get_json_as_perl($json);

        $self->valid_controlpanel_object($cp_data);

    }, 'api/v1/controlpanel/create: create a simple controlpanel');

    $zs->zs_transaction_ok(sub {
        $self->_prepare_controlpanel;
        my $mech     = $zs->mech;
        my $company  = $zs->create_bedrijf_ok;

        $mech->zs_login;
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/create',
            {
                owner            => 'betrokkene-bedrijf-' . $company->id,
                customer_type    => 'commercial',
                template         => 'fritsie',
            }
        );

        my $json     = $mech->content;
        my $cp_data  = $self->_get_json_as_perl($json);

        my $instance = $self->valid_controlpanel_object($cp_data);

        is($instance->{template}, 'fritsie', 'Got template: "fritsie"');
        is($instance->{customer_type}, 'commercial', 'Got customer_type: "commercial"');
    }, 'api/v1/controlpanel/create: create a simple controlpanel, different template');

    $zs->zs_transaction_ok(sub {
        $self->_prepare_controlpanel;
        my $mech     = $zs->mech;
        my $company  = $zs->create_bedrijf_ok;

        $mech->zs_login;
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/create',
            {
                owner             => 'betrokkene-bedrijf-' . $company->id,
                customer_type     => 'commercial',
                template          => 'fritsie',
                read_only         => 1,
                allowed_instances => 3,
                shortname         => 'mintlabb',
                customer_reference => 'case-782349234234',
            }
        );

        my $json     = $mech->content;
        my $cp_data  = $self->_get_json_as_perl($json);

        my $instance = $self->valid_controlpanel_object($cp_data);

        is($instance->{template}, 'fritsie', 'Got template: "fritsie"');
        is($instance->{customer_type}, 'commercial', 'Got customer_type: "commercial"');
        ok($instance->{read_only}, 'Created controlpanel is set readonly');
        is($instance->{allowed_instances}, 3, 'Allowed instances: 3');
        is($instance->{customer_reference}, 'case-782349234234', 'Got reference: "case-782349234234"');
    }, 'api/v1/controlpanel/create: create a scontrolpanel, optional features');
}

=head3 update controlpanel

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/c25d2daf-0e00-4fa5-8bc7-b61fff072234/update

B<Request>

=begin javascript

{
   "customer_type" : "com",
   "ipaddresses" : [
      "127.0.0.1"
   ],
   "template" : "fritsie"
}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-c055cf-c6d8c4",
   "result" : {
      "instance" : {
         "customer_type" : "government",
         "id" : "a6199d6f-86c9-49db-852c-f4fa284fa636",
         "ipaddresses" : [
            "127.0.0.1"
         ],
         "owner" : "betrokkene-bedrijf-253",
         "template" : "fritsie"
      },
      "reference" : "a6199d6f-86c9-49db-852c-f4fa284fa636",
      "type" : "controlpanel"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_controlpanel_update : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        $self->_prepare_controlpanel;
        my $mech     = $zs->mech;
        my $company  = $zs->create_bedrijf_ok;

        ### Create a controlpanel object
        $mech->zs_login;
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/create',
            {
                owner               => 'betrokkene-bedrijf-' . $company->id,
                customer_type       => 'government',
                ipaddresses         => ['10.44.0.11', '10.44.0.12'],
                customer_reference  => 'testcase',
                read_only           => 1,
            }
        );

        my $json     = $mech->content;
        my $cp_data  = $self->_get_json_as_perl($json);

        my $instance = $self->valid_controlpanel_object($cp_data);

        is($instance->{ipaddresses}->[0], '10.44.0.11', 'first address is  "10.44.0.11"');
        is($instance->{ipaddresses}->[1], '10.44.0.12', 'second address is "10.44.0.12"');
        ok($instance->{customer_reference}, 'Correctly setted customer reference');
        ok($instance->{read_only}, 'Correctly setted read_only to true');


        ### Update the controlpanel object
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $instance->{id} . '/update',
            {
                customer_type       => 'com',
                template            => 'fritsie',
                ipaddresses         => ['127.0.0.1'],
                read_only           => 0,
                customer_reference  => '',
            }
        );

        $json     = $mech->content;
        $cp_data  = $self->_get_json_as_perl($json);

        $instance = $self->valid_controlpanel_object($cp_data);

        is($instance->{template}, 'fritsie', 'template changed to "fritsie"');
        is($instance->{ipaddresses}->[0], '127.0.0.1', 'ip addresses list changed to "127.0.0.1"');
        is($instance->{customer_reference}, undef, 'Update: Empty customer reference');
        ok(!$instance->{read_only}, 'Update: read_only to false');

    }, 'api/v1/controlpanel/update: update a controlpanel');
}

=head2 Get

=head3 get controlpanel

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/8989asdf-asdf-asdf89a89sa-asdf89

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-81b8d1-0134cc",
   "result" : {
      "instance" : {
         "customer_type" : "government",
         "id" : "c25d2daf-0e00-4fa5-8bc7-b61fff072234",
         "ipaddresses" : [],
         "owner" : "betrokkene-bedrijf-131",
         "template" : "mintlab"
      },
      "reference" : "c25d2daf-0e00-4fa5-8bc7-b61fff072234",
      "type" : "controlpanel"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_controlpanel_get : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
         $self->_prepare_controlpanel;
         my $mech     = $zs->mech;
         my $company  = $zs->create_bedrijf_ok;

         $mech->zs_login;
         $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/create',
            {
               owner            => 'betrokkene-bedrijf-' . $company->id,
               customer_type    => 'government',
            }
         );

         my $json     = $mech->content;
         my $cp_data  = $self->_get_json_as_perl($json);

         my $r       = $mech->get('/api/v1/controlpanel/' . $cp_data->{result}->{reference});
         $json       = $mech->content;

         $self->valid_controlpanel_object($self->_get_json_as_perl($json));

         # print STDERR $json;

    }, 'api/v1/controlpanel/get: get a simple controlpanel');
}

=head2 List

=head3 get controlpanel listing

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-81b8d1-7e5dd3",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 2,
            "total_rows" : 2
         },
         "rows" : [
            {
               "instance" : {
                  "customer_type" : "government",
                  "id" : "c25d2daf-0e00-4fa5-8bc7-b61fff072234",
                  "ipaddresses" : [],
                  "owner" : "betrokkene-bedrijf-131",
                  "template" : "mintlab"
               },
               "reference" : "c25d2daf-0e00-4fa5-8bc7-b61fff072234",
               "type" : "controlpanel"
            },
            {
               "instance" : {
                  "customer_type" : "government",
                  "id" : "14b711c2-66b5-4785-85c8-b5c04fb9dcbf",
                  "ipaddresses" : [],
                  "owner" : "betrokkene-bedrijf-130",
                  "template" : "mintlab"
               },
               "reference" : "14b711c2-66b5-4785-85c8-b5c04fb9dcbf",
               "type" : "controlpanel"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_controlpanel_list : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
         $self->_prepare_controlpanel;
         my $mech     = $zs->mech;
         my $company  = $zs->create_bedrijf_ok;
         my $company2 = $zs->create_bedrijf_ok;

         $mech->zs_login;

         $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/create',
            {
               owner            => 'betrokkene-bedrijf-' . $company->id,
               customer_type    => 'government',
            }
         );

         $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/create',
            {
               owner            => 'betrokkene-bedrijf-' . $company2->id,
               customer_type    => 'government',
            }
         );

         my $r       = $mech->get('/api/v1/controlpanel');
         my $json    = $mech->content;

         my $cp_data = $self->_get_json_as_perl($json);

         is($cp_data->{result}->{type}, 'set', 'Got a set of answers');

         $self->valid_controlpanel_object($cp_data);

         #print STDERR $json;

    }, 'api/v1/controlpanel: list all of controlpanel objects of zaaksysteem');
}

=head2 Search

=head3 get controlpanel listing

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel?zql=select {} from controlpanel where owner = "betrokkene-bedrijf-24"

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-81b8d1-7e5dd3",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 2,
            "total_rows" : 2
         },
         "rows" : [
            {
               "instance" : {
                  "customer_type" : "government",
                  "id" : "c25d2daf-0e00-4fa5-8bc7-b61fff072234",
                  "ipaddresses" : [],
                  "owner" : "betrokkene-bedrijf-131",
                  "template" : "mintlab"
               },
               "reference" : "c25d2daf-0e00-4fa5-8bc7-b61fff072234",
               "type" : "controlpanel"
            },
            {
               "instance" : {
                  "customer_type" : "government",
                  "id" : "14b711c2-66b5-4785-85c8-b5c04fb9dcbf",
                  "ipaddresses" : [],
                  "owner" : "betrokkene-bedrijf-130",
                  "template" : "mintlab"
               },
               "reference" : "14b711c2-66b5-4785-85c8-b5c04fb9dcbf",
               "type" : "controlpanel"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_controlpanel_search : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
         $self->_prepare_controlpanel;
         my $mech     = $zs->mech;
         my $company  = $zs->create_bedrijf_ok;
         my $company2 = $zs->create_bedrijf_ok;

         $mech->zs_login;

         $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/create',
            {
               owner            => 'betrokkene-bedrijf-' . $company->id,
               customer_type    => 'government',
            }
         );

         $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/create',
            {
               owner            => 'betrokkene-bedrijf-' . $company2->id,
               customer_type    => 'government',
            }
         );

         my $r       = $mech->get('/api/v1/controlpanel?zql=select {} from controlpanel where owner="betrokkene-bedrijf-' . $company->id . '"');
         my $json    = $mech->content;

         my $cp_data = $self->_get_json_as_perl($json);

         is($cp_data->{result}->{type}, 'set', 'Got a set of answers');
         is($cp_data->{result}->{instance}->{pager}->{rows}, 1, 'Got a single row');

         my $instance = $self->valid_controlpanel_object($cp_data);
         is($instance->{owner}, 'betrokkene-bedrijf-' . $company->id, 'Got a single row with correct owner');

         #print STDERR $json;

    }, 'api/v1/controlpanel: list all of controlpanel objects of zaaksysteem');
}

=head1 IMPLEMENTATION TESTS

=head2 cat_api_v1_controlpanel_instance_exceptions

=cut

sub cat_api_v1_controlpanel_exceptions : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        #$self->_prepare_controlpanel;
        my $mech     = $zs->mech;
        my $company  = $zs->create_bedrijf_ok;

        ### No controlpanel
        $mech->zs_login;
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/create',
            {
                owner            => 'betrokkene-bedrijf-' . $company->id,
                customer_type    => 'government',
            },
        );
        $self->validate_json_exception($mech, qr/Controlpanel interface unavailable/, 'Exception: controlpanel interface unavailable');

        $self->_prepare_controlpanel;

        ### controlpanel exists
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/create',
            {
                owner            => 'betrokkene-bedrijf-' . $company->id,
                customer_type    => 'government',
            },
        );
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/create',
            {
                owner            => 'betrokkene-bedrijf-' . $company->id,
                customer_type    => 'government',
            },
        );

        $self->validate_json_exception($mech, qr/controlpanel already exists/, 'Exception: controlpanel already exists');

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/create',
            {
                owner            => 'betrokkene-bedrijf-987654',
                customer_type    => 'government',
            },
        );

        $self->validate_json_exception($mech, qr/controlpanel cannot be created on unknown owne/, 'Exception: controlpanel cannot be created on unknown owne');
    }, 'api/v1/controlpanel/create: exceptions');

    $zs->zs_transaction_ok(sub {
        $self->_prepare_controlpanel;
        my $mech     = $zs->mech;
        my $company  = $zs->create_bedrijf_ok;

        ### No controlpanel
        $mech->zs_login;
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/create',
            {
                owner            => 'betrokkene-bedrijf-' . $company->id,
                customer_type    => 'government',
            },
        );

        my $r       = $mech->get('/api/v1/controlpanel/123456');
        # my $r       = $mech->get('/api/v1/controlpanel/' . $cp_data->{result}->{reference});
        my $json    = $mech->content;
        my $cp_data = $self->_get_json_as_perl($json);

        is($mech->status, '404', 'Got valid exception error');
        is($cp_data->{status_code}, '404', 'Got valid status_code');
        is($cp_data->{result}->{type}, 'exception', 'Got exception for missing interface');
        like($cp_data->{result}->{instance}->{message}, qr/The controlpanel object with UUID.*not be found/, 'Exception: controlpanel could not be found');
    }, 'api/v1/controlpanel/get: exceptions');
}

sub validate_json_exception {
    my $self    = shift;
    my $mech    = shift;
    my $regex   = shift;
    my $message = shift;

    my $json     = $mech->content;
    my $cp_data  = $self->_get_json_as_perl($json);

    is($mech->status, '500', 'Got valid exception error');
    is($cp_data->{status_code}, '500', 'Got valid status_code');
    is($cp_data->{result}->{type}, 'exception', 'Got exception for missing interface');
    like($cp_data->{result}->{instance}->{message}, $regex, $message);
}


sub valid_controlpanel_object {
    my $self      = shift;
    my $perl      = shift;

    my $instance = $perl->{result}->{instance};
    if ($perl->{result}->{type} eq 'set') {
        $instance = $perl->{result}->{instance}->{rows}->[0]->{instance};
    } else {
       is($perl->{result}->{type}, 'controlpanel', 'Found controlpanel instance');
       like($perl->{result}->{reference}, qr/^\w+\-\w+\-/, 'Got a "reference", like "f87e9f29-5720-4743-9cc5-47887b342709"');
       isa_ok($perl->{result}->{instance}, 'HASH', 'Got an instance of controlpanel');
    }

    like($instance->{customer_type}, qr/^(?:government|commercial)$/, 'Got a valid value for customer_type');
    isa_ok($instance->{available_templates}, 'ARRAY', 'Got an array of templates');
    ok(@{ $instance->{available_templates} } > 5, 'Got at least 5 available templates');
    isa_ok($instance->{ipaddresses}, 'ARRAY', 'Got an array of ipaddresses');
    like($instance->{owner}, qr/^betrokkene-bedrijf-\d+$/, 'Got a valid value for owner');
    ok($instance->{template}, 'Got a value for template');

    return $instance;
}

sub _get_json_as_perl {
    my $self    = shift;
    my $json    = shift;

    my $jo      = JSON->new->utf8->pretty->canonical;

    return $jo->decode($json);
}

sub _prepare_controlpanel {
    my $self    = shift;

    $zs->create_interface_ok(
        module => 'controlpanel'
    );


}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
