package TestFor::General::StUF::0204::General;
use base qw(Test::Class);

use Moose;
use TestSetup;

sub stuf_0204_general_generate_stuurgegevens : Tests {
    my $class = Dummy::0204::General->new;

    my $input = {
        messagetype             => 'Lk01',
        entitytype              => 'PRS',
        sender                  => 'ZSNL',
        receiver                => 'CMODIS',
        reference               => 'ZS0000229154',
        datetime                => '2014030209011458',
        mutation_type           => 'create',
        follow_subscription     => 1,
    };

    my $output = {
        'berichtsoort' => 'Lk01',
        'entiteittype' => 'PRS',
        'kennisgeving' => {
            'indicatorOvername' => 'V',
            'mutatiesoort' => 'T'
        },
        'ontvanger' => {
            'applicatie' => 'CMODIS'
        },
        'referentienummer' => 'ZS0000229154',
        'sectormodel' => 'BG',
        'tijdstipBericht' => '2014030209011458',
        'versieSectormodel' => '0204',
        'versieStUF' => '0204',
        'zender' => {
            'applicatie' => 'ZSNL'
        }

    };

    throws_ok(
        sub {$class->generate_stuurgegevens },
        qr/Validation of profile failed/,
        'Profile validates invalid'
    );

    is_deeply(
        $class->generate_stuurgegevens($input),
        $output,
        'Correct generation of stuurgegevens'
    );
}

sub stuf_0204_general_get_mutation_type_from_stuurgegevens : Tests {
    my $class = Dummy::0204::General->new;

    my $stuur = {
        'berichtsoort' => 'Lk01',
        'entiteittype' => 'PRS',
        'kennisgeving' => {
            'indicatorOvername' => 'V',
            'mutatiesoort' => 'T'
        },
        'ontvanger' => {
            'applicatie' => 'CMODIS'
        },
        'referentienummer' => 'ZS0000229154',
        'sectormodel' => 'BG',
        'tijdstipBericht' => '2014030209011458',
        'versieSectormodel' => '0204',
        'versieStUF' => '0204',
        'zender' => {
            'applicatie' => 'ZSNL'
        }
    };

    is($class->_get_mutation_type_from_stuurgegevens($stuur), 'create', 'Found "create" mutation for "T"');
    $stuur->{kennisgeving}->{mutatiesoort} = 'W';
    is($class->_get_mutation_type_from_stuurgegevens($stuur), 'update', 'Found "update" mutation for "W"');
    $stuur->{kennisgeving}->{mutatiesoort} = 'V';
    is($class->_get_mutation_type_from_stuurgegevens($stuur), 'delete', 'Found "delete" mutation for "V"');
    $stuur->{kennisgeving}->{mutatiesoort} = 'C';
    is($class->_get_mutation_type_from_stuurgegevens($stuur), 'update', 'Found "update" mutation for "C"');

    $stuur->{kennisgeving}->{mutatiesoort} = 'X';
    ok(!$class->_get_mutation_type_from_stuurgegevens($stuur), 'Got undef for "X"');

}

package Dummy::0204::General;

use Moose;
with 'Zaaksysteem::StUF::0204::General';


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

