package Zaaksysteem::Test::API::v1::CasetypeACL;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::API::v1::CasetypeACL;
use Zaaksysteem::Test;

sub test_get_casetype_users {
    my @casetype_acl_entries = (
        mock_one(
            casetype_uuid   => 'aa14fe0c-5ff6-11e8-b5ae-e3015786d202',
            casetype_active => 1,

            confidential => 0,
            capability   => 'manage',
            username     => 'piet',
        ),
        mock_one(
            casetype_uuid   => 'aa14fe0c-5ff6-11e8-b5ae-e3015786d202',
            casetype_active => 1,

            confidential => 0,
            capability   => 'manage',
            username     => 'kees',
        ),
        mock_one(
            casetype_uuid   => 'f12d9654-5ff7-11e8-ace1-6385f186afb8',
            casetype_active => 1,

            confidential => 1,
            capability   => 'manage',
            username     => 'piet',
        ),
        mock_one(
            casetype_uuid   => 'aa14fe0c-5ff6-11e8-b5ae-e3015786d202',
            casetype_active => 1,

            confidential => 1,
            capability   => 'manage',
            username     => 'piet',
        ),
    );

    my $mock_self = mock_one(
        schema => sub {
            return mock_one(
                resultset => sub {
                    my $rs_name = shift;

                    if ($rs_name eq 'CasetypeACL') {
                        return mock_one(
                            next => sub {
                                my $cae = shift @casetype_acl_entries;
                                return $cae;
                            },
                        );
                    } else {
                        die("Unknown resultset request: $rs_name");
                    }
                }
            );
        },
    );

    {
        my $rv = Zaaksysteem::API::v1::CasetypeACL::get_casetype_users(
            $mock_self, 
        );

        is(@$rv, 2, 'get_casetype_users returns 2 casetypes');
        is(
            $rv->[0]->casetype_uuid,
            'aa14fe0c-5ff6-11e8-b5ae-e3015786d202',
            "Lowest-sorting casetype UUID comes first"
        );
        cmp_deeply(
            $rv->[0]->public,
            {
                manage => [qw(piet kees)],
                read   => [],
            },
            'manage permissions correctly set on public side of casetype ACL',
        );
        cmp_deeply(
            $rv->[0]->trusted,
            {
                manage => [qw(piet)],
                read   => [],
            },
            'manage permissions correctly set on trusted side of casetype ACL',
        );

        is(
            $rv->[1]->casetype_uuid,
            'f12d9654-5ff7-11e8-ace1-6385f186afb8',
            "Highest-sorting casetype UUID comes first"
        );
        cmp_deeply(
            $rv->[1]->public,
            {
                read   => [],
            },
            'manage permissions correctly set on public side of casetype ACL',
        );
        cmp_deeply(
            $rv->[1]->trusted,
            {
                manage => [qw(piet)],
                read   => [],
            },
            'manage permissions correctly set on trusted side of casetype ACL',
        );
    }
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
