package Zaaksysteem::Test::Backend::Sysin::Modules::STUFNPS;
use Moose;

extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::Backend::Sysin::Modules::STUFNPS;

=head1 NAME

Zaaksysteem::Test::Backend::Sysin::Modules::STUFNPS - Test StUF NPS interactions

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Backend::Sysin::Modules::STUFNPS

=cut

sub test__get_xml_processor {
    my $config = {
        gemeentecode  => '31337',
    };
    my $call_options = {
        reference => '42',
        receiver => 'them',
        sender   => 'us',
    };
    my $current_user = mock_one(
        username => sub { 'piet' },
    );

    my $fake_schema = mock_one(
        current_user => sub {
            return $current_user;
        }
    );
    my $fake_transaction = mock_one(
        result_source => sub {
            return mock_one(
                schema => sub { $fake_schema },
            );
        },
        interface_id => sub {
            return mock_one(
                uuid => sub { '4b176779-dc09-49a8-ad86-87f29b78a504' },
            );
        }
    );

    my $fake_self = mock_one(
        _load_call_options_from_config => sub {
            return $call_options;
        },
        get_config_from_config_iface => sub {
            return $config;
        },
    );

    my $override = override(
        'Zaaksysteem::StUF::0301::Processor::BG0310::new',
        sub {
            return {_class => @_};
        }
    );

    # Centric: sender contains username
    {
        $config->{stuf_supplier} = 'centric';

        my $processor = Zaaksysteem::Backend::Sysin::Modules::STUFNPS::_get_xml_processor(
            $fake_self,
            transaction => $fake_transaction,
        );

        delete $processor->{schema};
        cmp_deeply(
            $processor,
            {
                _class => 'Zaaksysteem::StUF::0301::Processor::BG0310',
                interface_uuid => '4b176779-dc09-49a8-ad86-87f29b78a504',
                reference_id   => '42',
                our_party => {
                    applicatie => 'us',
                    gebruiker  => 'piet',
                },
                other_party => {
                    applicatie => 'them',
                },
                other_party_primary_key => 'sleutelVerzendend',
                municipality_code => '31337',
            },
            'Centric StUF supplier, known username = username in StUF header'
        );

        my $old_current_user = $current_user;
        $current_user = undef;

        $processor = Zaaksysteem::Backend::Sysin::Modules::STUFNPS::_get_xml_processor(
            $fake_self,
            transaction => $fake_transaction,
        );

        delete $processor->{schema};
        cmp_deeply(
            $processor,
            {
                _class => 'Zaaksysteem::StUF::0301::Processor::BG0310',
                interface_uuid => '4b176779-dc09-49a8-ad86-87f29b78a504',
                reference_id   => '42',
                our_party => {
                    applicatie => 'us',
                    gebruiker  => 'unknown',
                },
                other_party => {
                    applicatie => 'them',
                },
                other_party_primary_key => 'sleutelVerzendend',
                municipality_code => '31337',
            },
            'Centric StUF supplier, no username = unknown in StUF header'
        );

        $current_user = $old_current_user;
    }

    # Pink: sender contains username
    {
        $config->{stuf_supplier} = 'pinkv3';

        my $processor = Zaaksysteem::Backend::Sysin::Modules::STUFNPS::_get_xml_processor(
            $fake_self,
            transaction => $fake_transaction,
        );

        delete $processor->{schema};
        cmp_deeply(
            $processor,
            {
                _class => 'Zaaksysteem::StUF::0301::Processor::BG0310',
                interface_uuid => '4b176779-dc09-49a8-ad86-87f29b78a504',
                reference_id   => '42',
                our_party => {
                    applicatie => 'us',
                },
                other_party => {
                    applicatie => 'them',
                },
                other_party_primary_key => 'sleutelVerzendend',
                municipality_code => '31337',
            },
            'Other StUF supplier, known username = no username in StUF header'
        );

        $current_user = undef;

        $processor = Zaaksysteem::Backend::Sysin::Modules::STUFNPS::_get_xml_processor(
            $fake_self,
            transaction => $fake_transaction,
        );

        delete $processor->{schema};
        cmp_deeply(
            $processor,
            {
                _class => 'Zaaksysteem::StUF::0301::Processor::BG0310',
                interface_uuid => '4b176779-dc09-49a8-ad86-87f29b78a504',
                reference_id   => '42',
                our_party => {
                    applicatie => 'us',
                },
                other_party => {
                    applicatie => 'them',
                },
                other_party_primary_key => 'sleutelVerzendend',
                municipality_code => '31337',
            },
            'Other StUF supplier, no username = no username in StUF header'
        );
    }

}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
