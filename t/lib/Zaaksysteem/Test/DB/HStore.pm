use utf8;
package Zaaksysteem::Test::DB::HStore;

use Moose;
extends 'Zaaksysteem::Test::Moose';

=head1 NAME

Zaaksysteem::Test::DB::HStore - Test Zaaksysteem::DB::HStore

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::DB::HStore

=cut

use Zaaksysteem::Test;
use Zaaksysteem::Test::DB::HStore;

sub test_hstore_encode {
    is(
        Zaaksysteem::DB::HStore::encode({ a => 'b'}),
        '"a"=>"b"',
        'Simple hstore with single key'
    );
    is(
        Zaaksysteem::DB::HStore::encode({ a => 'b', c => 'd' }),
        '"a"=>"b","c"=>"d"',
        'Simple hstore with two keys'
    );
    is(
        Zaaksysteem::DB::HStore::encode({ a => 'b\\\\"', c => 'd"""' }),
        # \\\\Pfffff\\\\
        '"a"=>"b\\\\\\\\\\"","c"=>"d\\"\\"\\""',
        'hstore with two keys with characters that need escaping'
    );
    is(
        Zaaksysteem::DB::HStore::encode({ a => 'á', '☃' => '🔥' }),
        '"a"=>"á","☃"=>"🔥"',
        'Unicode characters are not mangled'
    );
    is(
        Zaaksysteem::DB::HStore::encode({ a => 'NULL', b => undef }),
        '"a"=>"NULL","b"=>NULL',
        'String "NULL" and undef are not confused'
    );
}

sub test_hstore_decode {
    cmp_deeply(
        Zaaksysteem::DB::HStore::decode('"a"=>"b"'),
        { a => 'b' },
        'hstore with single key, quoted'
    );
    cmp_deeply(
        Zaaksysteem::DB::HStore::decode('"a" => "b"'),
        { a => 'b' },
        'hstore with single key, quoted, space around arrow',
    );
    cmp_deeply(
        Zaaksysteem::DB::HStore::decode('a=>b'),
        { a => 'b' },
        'hstore with single key, unquoted'
    );
    cmp_deeply(
        Zaaksysteem::DB::HStore::decode('a => b'),
        { a => 'b' },
        'hstore with single key, unquoted, space around arrow'
    );
    cmp_deeply(
        Zaaksysteem::DB::HStore::decode('a => NULL, b => "NULL"'),
        { a => undef, b => "NULL" },
        'unquoted NULL is undef, quoted NULL is string "NULL"'
    );
    cmp_deeply(
        Zaaksysteem::DB::HStore::decode('"a"=>"b\\\""'),
        { a => 'b\\"' },
        'hstore with escaped characters in value'
    );
    cmp_deeply(
        Zaaksysteem::DB::HStore::decode('"a"=>"b\\\""  '),
        { a => 'b\\"' },
        'hstore with escaped characters in value, followed by whitespace only'
    );
    cmp_deeply(
        Zaaksysteem::DB::HStore::decode('"a"=>"b\\\""  , c => "d"'),
        { a => 'b\\"', c => 'd' },
        'hstore with escaped characters in value, followed by another key/value pair'
    );
    cmp_deeply(
        Zaaksysteem::DB::HStore::decode('"a\""=>"b"'),
        { 'a"' => 'b' },
        'hstore with escaped characters in key',
    );
    cmp_deeply(
        Zaaksysteem::DB::HStore::decode('"a"=>"á","☃"=>"🔥"'),
        { a => 'á', '☃' => '🔥' },
        'Keys and values with unicode characters work as expected',
    );
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
