package Zaaksysteem::Test::Document::Model;

use Moose;
extends "Zaaksysteem::Test::Moose";

=head1 NAME

Zaaksysteem::Test::Document::Model - Test Zaaksysteem::Test::Document::Model

=head1 DESCRIPTION

Test the new document model used by api/v1

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Document::Model

=cut

use BTTW::Tools::RandomData qw(:uuid);
use Data::Random::NL qw(:all);
use DateTime;
use List::Util qw(all);
use Zaaksysteem::Document::Model;
use Zaaksysteem::Test;

sub test_pod {
    pod_coverage_ok('Zaaksysteem::Document::Model');
}

sub _get_model {
    my $model = Zaaksysteem::Document::Model->new(
        rs_file      => mock_one(),
        rs_filestore => mock_one(),
        subject      => mock_one(),
        storage      => mock_one(),
        @_,
    );
}

sub test_generate_serial_number {
    my $model = _get_model(
        rs_file => mock_strict(
            generate_file_id => 42,
        ),
    );

    my $serial = $model->generate_serial_number;

    is($serial->serial, 42,
        "Generate file ID is passed to generate_file_id on rs_file resultset"
    );

    is($serial->object_class, 'document',
        ".. and the object class is 'document'",
    );

    is($serial->name, 'number',
        ".. and the name is 'number'",
    );
}

sub test_list {

    my $model = _get_model();

    $model->list;
    pass("list works");
}

sub mock_file {
    my $file = mock_one(
        update_file => sub {
            my $args = shift;
            return mock_one(
                %$args,
                'X-Mock-SelfArg'  => 1,
                update_properties => sub {
                    my $self = shift;
                    my $args = shift;
                    foreach (keys %$args) {
                        $self->{$_} = $args->{$_};
                    }
                },
            );
        },
    );
    return $file;
}

sub test_create_new_version {

    my $called_as_object = 0;
    my $subject          = mock_one(
        'X-Mock-Stringify' => "My Subject",
        as_object          => sub {
            $called_as_object++;
        },
    );

    my $model = _get_model(subject => $subject);

    my $file = mock_file();

    my $new = $model->create_new_version(
        $file,
        path     => '/tmp/my/fake/file',
        filename => 'My Fake File',
    );

    ok($new, "We have a new file");

    # update_file
    is($new->new_file_path, '/tmp/my/fake/file', '.. with the correct path');
    is($new->original_name, 'My Fake File', '.. and the correct filename');
    is($called_as_object, 1, ".. and subject was called as_object once");

    # update_properties
    is($new->subject . "", "My Subject", ".. and has the correct subject");
    is($new->accepted,     1,            ".. and has been accepted");
}

sub test_update_metadata {

    my $subject          = mock_one(
        'X-Mock-Stringify' => "My Subject",
    );
    my $model = _get_model(subject => $subject);

    my $file = mock_one(
        update_properties => sub {
            my $args = shift;
            is($args->{subject} . "",
                "My Subject",
                "Update properties called with correct subject");
            is(
                $args->{document_status} => "Computer says yes",
                ".. and correct document status"
            );
        },
        update_metadata => sub {
            my $args = shift;
            cmp_deeply($args, 
                {
                    document_category => 'Category',
                    pronom_format => 'fmt/PRONOM',
                    origin        => 'Rainbow',
                    trust_level   => "Confidential",
                    description   => 'How about no?',
                    appearance    => "In testsuite",
                    structure     => "TAP",
                },
                "update_metadata called with correct params");

        },
    );

    my %metadata = (
        category      => 'Category',
        pronom_format => 'fmt/PRONOM',
        origin        => 'Rainbow',
        trust_level   => "Confidential",
        description   => 'How about no?',
        appearance    => "In testsuite",
        structure     => "TAP",
        status        => "Computer says yes",
    );

    my $foo = $model->update_metadata(
        $file,
        %metadata,
    );

    $file = mock_one(
        update_metadata => sub {
            my $args = shift;
            cmp_deeply($args,
                {
                    document_category      => undef,
                },
                "update_metadata called with correct param (undef)");
        },
    );

    my $bar = $model->update_metadata(
        $file,
        category => undef,
    );

}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
